class CreateMailTemplates < ActiveRecord::Migration
  def change
    create_table :mail_templates do |t|
      t.text :template
      t.string :mensaje
      t.string :titulo

      t.timestamps
    end
  end
end
