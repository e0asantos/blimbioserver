class CreateAdserviceusers < ActiveRecord::Migration
  def change
    create_table :adserviceusers do |t|
      t.integer :userid

      t.timestamps
    end
  end
end
