class CreateBeaconHistories < ActiveRecord::Migration
  def change
    create_table :beacon_histories do |t|
      t.integer :entraces_id
      t.integer :beacons_id
      t.string :flux

      t.timestamps
    end
  end
end
