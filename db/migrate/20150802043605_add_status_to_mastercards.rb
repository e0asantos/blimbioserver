class AddStatusToMastercards < ActiveRecord::Migration
  def change
    add_column :mastercards, :status, :integer
  end
end
