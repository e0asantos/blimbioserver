class CreatePlastics < ActiveRecord::Migration
  def change
    create_table :plastics do |t|
      t.string :card_number
      t.string :ccv
      t.string :expiration
      t.integer :user_id
      t.string :tokenized

      t.timestamps
    end
  end
end
