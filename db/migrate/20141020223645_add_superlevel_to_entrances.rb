class AddSuperlevelToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :superlevel, :integer
  end
end
