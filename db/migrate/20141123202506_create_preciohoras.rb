class CreatePreciohoras < ActiveRecord::Migration
  def change
    create_table :preciohoras do |t|
      t.integer :hora
      t.integer :plan
      t.string :precio

      t.timestamps
    end
  end
end
