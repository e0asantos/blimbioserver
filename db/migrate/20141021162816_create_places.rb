class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :nombre
      t.string :pais

      t.timestamps
    end
  end
end
