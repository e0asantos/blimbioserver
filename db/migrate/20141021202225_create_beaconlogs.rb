class CreateBeaconlogs < ActiveRecord::Migration
  def change
    create_table :beaconlogs do |t|
      t.integer :beacons_id
      t.integer :client_id
      t.integer :campaing_id

      t.timestamps
    end
  end
end
