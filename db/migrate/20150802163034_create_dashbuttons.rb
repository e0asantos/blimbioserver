class CreateDashbuttons < ActiveRecord::Migration
  def change
    create_table :dashbuttons do |t|
      t.integer :id_owner
      t.string :id_servicio

      t.timestamps
    end
  end
end
