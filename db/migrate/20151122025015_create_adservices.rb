class CreateAdservices < ActiveRecord::Migration
  def change
    create_table :adservices do |t|
      t.string :name
      t.integer :age_from
      t.integer :age_to
      t.string :content_type
      t.string :url_content
      t.string :genre

      t.timestamps
    end
  end
end
