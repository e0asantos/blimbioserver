class CreatePushes < ActiveRecord::Migration
  def change
    create_table :pushes do |t|
      t.string :channel
      t.string :event
      t.string :user_list

      t.timestamps
    end
  end
end
