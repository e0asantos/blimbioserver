class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :channel
      t.string :screen
      t.string :dato
      t.string :users

      t.timestamps
    end
  end
end
