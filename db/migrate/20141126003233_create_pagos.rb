class CreatePagos < ActiveRecord::Migration
  def change
    create_table :pagos do |t|
      t.datetime :fechainicio
      t.datetime :fechafin
      t.integer :usuarios_id
      t.string :estado_payment
      t.string :cantidad_original
      t.string :cantidad_final
      t.integer :campaign_id

      t.timestamps
    end
  end
end
