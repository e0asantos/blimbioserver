class AddTokenToDashbuttons < ActiveRecord::Migration
  def change
    add_column :dashbuttons, :token, :string
  end
end
