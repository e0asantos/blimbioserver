class AddEndDateToBeaconlogs < ActiveRecord::Migration
  def change
    add_column :beaconlogs, :end_date, :datetime
  end
end
