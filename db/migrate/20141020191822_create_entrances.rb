class CreateEntrances < ActiveRecord::Migration
  def change
    create_table :entrances do |t|
      t.string :nombre
      t.string :apellido
      t.string :email
      t.string :empresa
      t.string :rfc

      t.timestamps
    end
  end
end
