class RemoveSuperlevelFromEntrances < ActiveRecord::Migration
  def change
    remove_column :entrances, :superlevel, :integer
  end
end
