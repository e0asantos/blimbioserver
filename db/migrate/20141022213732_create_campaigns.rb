class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :nombre
      t.integer :entrances_id
      t.integer :impresiones_hechas
      t.integer :impresiones_vistas
      t.integer :impresiones_meta
      t.string :descripcion
      t.string :imagen
      t.integer :justonce
      t.integer :tiempo_permanencia

      t.timestamps
    end
  end
end
