class AddFulldescriptionToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :fulldescription, :string
  end
end
