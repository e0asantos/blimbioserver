class CreateServicios < ActiveRecord::Migration
  def change
    create_table :servicios do |t|
      t.string :uid
      t.string :description
      t.string :price
      t.integer :owner_id
      t.string :tip
      t.integer :category

      t.timestamps
    end
  end
end
