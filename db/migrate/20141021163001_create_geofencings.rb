class CreateGeofencings < ActiveRecord::Migration
  def change
    create_table :geofencings do |t|
      t.string :nombre
      t.integer :entrance_id
      t.string :area

      t.timestamps
    end
  end
end
