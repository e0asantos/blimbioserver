class AddColorToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :color, :string
  end
end
