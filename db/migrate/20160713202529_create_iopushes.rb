class CreateIopushes < ActiveRecord::Migration
  def change
    create_table :iopushes do |t|
      t.string :device_id
      t.integer :user_id
      t.string :token

      t.timestamps null: false
    end
  end
end
