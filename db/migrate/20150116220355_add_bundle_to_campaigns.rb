class AddBundleToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :bundle, :integer
  end
end
