class CreatePrecios < ActiveRecord::Migration
  def change
    create_table :precios do |t|
      t.string :nombre
      t.integer :dias
      t.string :precio
      t.string :precioperpush

      t.timestamps
    end
  end
end
