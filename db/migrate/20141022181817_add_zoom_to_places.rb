class AddZoomToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :zoom, :string
  end
end
