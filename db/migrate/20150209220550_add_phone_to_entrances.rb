class AddPhoneToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :phone, :string
  end
end
