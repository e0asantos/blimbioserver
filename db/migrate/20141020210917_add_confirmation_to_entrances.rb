class AddConfirmationToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :confirmation, :string
  end
end
