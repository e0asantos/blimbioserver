class CreatePublicidads < ActiveRecord::Migration
  def change
    create_table :publicidads do |t|
      t.string :encabezado
      t.string :imagen

      t.timestamps
    end
  end
end
