class AddResetcodeToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :resetcode, :string
  end
end
