class AddAddressToEntrances < ActiveRecord::Migration
  def change
    add_column :entrances, :address, :string
  end
end
