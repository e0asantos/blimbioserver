class AddOwnerToBeacons < ActiveRecord::Migration
  def change
    add_column :beacons, :owner, :integer
  end
end
