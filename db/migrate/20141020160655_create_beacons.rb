class CreateBeacons < ActiveRecord::Migration
  def change
    create_table :beacons do |t|
      t.string :nombre
      t.string :latitud
      t.string :longitud
      t.string :radio
      t.string :uid
      t.string :uid_low
      t.string :uid_high

      t.timestamps
    end
  end
end
