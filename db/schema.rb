# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160713202529) do

  create_table "adservices", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.integer  "age_from",     limit: 4
    t.integer  "age_to",       limit: 4
    t.string   "content_type", limit: 255
    t.string   "url_content",  limit: 255
    t.string   "genre",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "adserviceusers", force: :cascade do |t|
    t.integer  "userid",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "beacon",     limit: 255
  end

  create_table "beacon_histories", force: :cascade do |t|
    t.integer  "entraces_id", limit: 4
    t.integer  "beacons_id",  limit: 4
    t.string   "flux",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "beaconlogs", force: :cascade do |t|
    t.integer  "beacons_id",   limit: 4
    t.integer  "client_id",    limit: 4
    t.integer  "campaing_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "activo",       limit: 4
    t.datetime "rewardeddate"
    t.string   "uidevice",     limit: 255
    t.integer  "is_product",   limit: 4
    t.datetime "end_date"
  end

  create_table "beacons", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.string   "latitud",     limit: 255
    t.string   "longitud",    limit: 255
    t.string   "radio",       limit: 255
    t.string   "uid",         limit: 255
    t.string   "uid_low",     limit: 255
    t.string   "uid_high",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "descripcion", limit: 255
    t.integer  "ciudad",      limit: 4
    t.integer  "activo",      limit: 4
    t.integer  "owner",       limit: 4
    t.integer  "precios_id",  limit: 4
    t.integer  "ijjoapp",     limit: 4
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "nombre",             limit: 255
    t.integer  "entrances_id",       limit: 4
    t.integer  "impresiones_hechas", limit: 4
    t.integer  "impresiones_vistas", limit: 4
    t.integer  "impresiones_meta",   limit: 4
    t.string   "descripcion",        limit: 255
    t.string   "imagen",             limit: 255
    t.integer  "justonce",           limit: 4
    t.integer  "tiempo_permanencia", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "activo",             limit: 4
    t.string   "promocode",          limit: 255
    t.integer  "cupon",              limit: 4
    t.string   "desactivacion",      limit: 255
    t.integer  "agefrom",            limit: 4,   default: 0
    t.integer  "ageto",              limit: 4,   default: 99
    t.integer  "sex",                limit: 4,   default: 0
    t.integer  "stoprewardsat",      limit: 4,   default: 0
    t.datetime "stoprewardsdate"
    t.integer  "visibleonsite",      limit: 4
    t.integer  "bundle",             limit: 4
    t.integer  "onexit",             limit: 4
    t.string   "systemmsg",          limit: 255
    t.string   "price",              limit: 255
    t.integer  "duration",           limit: 4
    t.integer  "parkinglot",         limit: 4
  end

  create_table "dashbuttons", force: :cascade do |t|
    t.integer  "id_owner",    limit: 4
    t.string   "id_servicio", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "token",       limit: 255
  end

  create_table "entrances", force: :cascade do |t|
    t.string   "nombre",           limit: 255
    t.string   "apellido",         limit: 255
    t.string   "email",            limit: 255
    t.string   "empresa",          limit: 255
    t.string   "rfc",              limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "confirmation",     limit: 255
    t.string   "resetcode",        limit: 255
    t.string   "hexicode",         limit: 255
    t.integer  "muro",             limit: 4
    t.datetime "birthday"
    t.string   "sexo",             limit: 255
    t.integer  "superlevel",       limit: 4,   default: 0
    t.integer  "isclient",         limit: 4
    t.string   "shortdescription", limit: 255
    t.string   "fulldescription",  limit: 255
    t.string   "phone",            limit: 255
    t.string   "horario",          limit: 255
    t.string   "address",          limit: 255
    t.string   "color",            limit: 255
    t.string   "marca",            limit: 255
    t.string   "modelo",           limit: 255
    t.string   "placa",            limit: 255
  end

  create_table "geofencings", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.integer  "entrance_id", limit: 4
    t.string   "area",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "iopushes", force: :cascade do |t|
    t.string   "device_id",  limit: 255
    t.integer  "user_id",    limit: 4
    t.string   "token",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "campaign_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mail_templates", force: :cascade do |t|
    t.text     "template",   limit: 65535
    t.string   "mensaje",    limit: 255
    t.string   "titulo",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "evento",     limit: 255
  end

  create_table "mastercards", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "id_user",     limit: 4
    t.integer  "servicio_id", limit: 4
    t.integer  "status",      limit: 4
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "channel",    limit: 255
    t.string   "screen",     limit: 255
    t.string   "dato",       limit: 255
    t.string   "users",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pagos", force: :cascade do |t|
    t.datetime "fechainicio"
    t.datetime "fechafin"
    t.integer  "usuarios_id",       limit: 4
    t.string   "estado_payment",    limit: 255
    t.string   "cantidad_original", limit: 255
    t.string   "cantidad_final",    limit: 255
    t.integer  "campaign_id",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "beacons_id",        limit: 4
    t.string   "paymentuid",        limit: 255
    t.string   "credito",           limit: 255
    t.string   "oxxo",              limit: 255
    t.string   "seveneleven",       limit: 255
    t.string   "olii",              limit: 255
    t.string   "guide",             limit: 255
    t.string   "cantidad",          limit: 255
    t.integer  "campaign_product",  limit: 4
    t.integer  "beaconlog_id",      limit: 4
  end

  create_table "places", force: :cascade do |t|
    t.string   "nombre",     limit: 255
    t.string   "pais",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "longitude",  limit: 255
    t.string   "zoom",       limit: 255
    t.string   "latitude",   limit: 255
  end

  create_table "plastics", force: :cascade do |t|
    t.string   "card_number", limit: 255
    t.string   "ccv",         limit: 255
    t.string   "expiration",  limit: 255
    t.integer  "user_id",     limit: 4
    t.string   "tokenized",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "preciohoras", force: :cascade do |t|
    t.integer  "hora",       limit: 4
    t.integer  "plan",       limit: 4
    t.string   "precio",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "horafin",    limit: 4
  end

  create_table "precios", force: :cascade do |t|
    t.string   "nombre",        limit: 255
    t.integer  "dias",          limit: 4
    t.string   "precio",        limit: 255
    t.string   "precioperpush", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "beacons_id",    limit: 4
    t.integer  "olii",          limit: 4
  end

  create_table "publicidads", force: :cascade do |t|
    t.string   "encabezado",   limit: 255
    t.string   "imagen",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "entrances_id", limit: 4
  end

  create_table "pushes", force: :cascade do |t|
    t.string   "channel",    limit: 255
    t.string   "event",      limit: 255
    t.string   "user_list",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "servicios", force: :cascade do |t|
    t.string   "uid",         limit: 255
    t.string   "description", limit: 255
    t.string   "price",       limit: 255
    t.integer  "owner_id",    limit: 4
    t.string   "tip",         limit: 255
    t.integer  "category",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "asignado",    limit: 4
  end

  create_table "welcomes", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
