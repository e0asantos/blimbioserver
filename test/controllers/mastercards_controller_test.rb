require 'test_helper'

class MastercardsControllerTest < ActionController::TestCase
  setup do
    @mastercard = mastercards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mastercards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mastercard" do
    assert_difference('Mastercard.count') do
      post :create, mastercard: {  }
    end

    assert_redirected_to mastercard_path(assigns(:mastercard))
  end

  test "should show mastercard" do
    get :show, id: @mastercard
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mastercard
    assert_response :success
  end

  test "should update mastercard" do
    patch :update, id: @mastercard, mastercard: {  }
    assert_redirected_to mastercard_path(assigns(:mastercard))
  end

  test "should destroy mastercard" do
    assert_difference('Mastercard.count', -1) do
      delete :destroy, id: @mastercard
    end

    assert_redirected_to mastercards_path
  end
end
