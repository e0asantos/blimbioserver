require 'test_helper'

class PlasticsControllerTest < ActionController::TestCase
  setup do
    @plastic = plastics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plastics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plastic" do
    assert_difference('Plastic.count') do
      post :create, plastic: { card_number: @plastic.card_number, ccv: @plastic.ccv, expiration: @plastic.expiration, tokenized: @plastic.tokenized, user_id: @plastic.user_id }
    end

    assert_redirected_to plastic_path(assigns(:plastic))
  end

  test "should show plastic" do
    get :show, id: @plastic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @plastic
    assert_response :success
  end

  test "should update plastic" do
    patch :update, id: @plastic, plastic: { card_number: @plastic.card_number, ccv: @plastic.ccv, expiration: @plastic.expiration, tokenized: @plastic.tokenized, user_id: @plastic.user_id }
    assert_redirected_to plastic_path(assigns(:plastic))
  end

  test "should destroy plastic" do
    assert_difference('Plastic.count', -1) do
      delete :destroy, id: @plastic
    end

    assert_redirected_to plastics_path
  end
end
