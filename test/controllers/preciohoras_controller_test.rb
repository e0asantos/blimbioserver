require 'test_helper'

class PreciohorasControllerTest < ActionController::TestCase
  setup do
    @preciohora = preciohoras(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:preciohoras)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create preciohora" do
    assert_difference('Preciohora.count') do
      post :create, preciohora: { hora: @preciohora.hora, plan: @preciohora.plan, precio: @preciohora.precio }
    end

    assert_redirected_to preciohora_path(assigns(:preciohora))
  end

  test "should show preciohora" do
    get :show, id: @preciohora
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @preciohora
    assert_response :success
  end

  test "should update preciohora" do
    patch :update, id: @preciohora, preciohora: { hora: @preciohora.hora, plan: @preciohora.plan, precio: @preciohora.precio }
    assert_redirected_to preciohora_path(assigns(:preciohora))
  end

  test "should destroy preciohora" do
    assert_difference('Preciohora.count', -1) do
      delete :destroy, id: @preciohora
    end

    assert_redirected_to preciohoras_path
  end
end
