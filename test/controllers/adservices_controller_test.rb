require 'test_helper'

class AdservicesControllerTest < ActionController::TestCase
  setup do
    @adservice = adservices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:adservices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create adservice" do
    assert_difference('Adservice.count') do
      post :create, adservice: { age_from: @adservice.age_from, age_to: @adservice.age_to, content_type: @adservice.content_type, genre: @adservice.genre, name: @adservice.name, url_content: @adservice.url_content }
    end

    assert_redirected_to adservice_path(assigns(:adservice))
  end

  test "should show adservice" do
    get :show, id: @adservice
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @adservice
    assert_response :success
  end

  test "should update adservice" do
    patch :update, id: @adservice, adservice: { age_from: @adservice.age_from, age_to: @adservice.age_to, content_type: @adservice.content_type, genre: @adservice.genre, name: @adservice.name, url_content: @adservice.url_content }
    assert_redirected_to adservice_path(assigns(:adservice))
  end

  test "should destroy adservice" do
    assert_difference('Adservice.count', -1) do
      delete :destroy, id: @adservice
    end

    assert_redirected_to adservices_path
  end
end
