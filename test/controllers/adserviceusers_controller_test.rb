require 'test_helper'

class AdserviceusersControllerTest < ActionController::TestCase
  setup do
    @adserviceuser = adserviceusers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:adserviceusers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create adserviceuser" do
    assert_difference('Adserviceuser.count') do
      post :create, adserviceuser: { userid: @adserviceuser.userid }
    end

    assert_redirected_to adserviceuser_path(assigns(:adserviceuser))
  end

  test "should show adserviceuser" do
    get :show, id: @adserviceuser
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @adserviceuser
    assert_response :success
  end

  test "should update adserviceuser" do
    patch :update, id: @adserviceuser, adserviceuser: { userid: @adserviceuser.userid }
    assert_redirected_to adserviceuser_path(assigns(:adserviceuser))
  end

  test "should destroy adserviceuser" do
    assert_difference('Adserviceuser.count', -1) do
      delete :destroy, id: @adserviceuser
    end

    assert_redirected_to adserviceusers_path
  end
end
