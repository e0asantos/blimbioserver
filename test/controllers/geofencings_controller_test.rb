require 'test_helper'

class GeofencingsControllerTest < ActionController::TestCase
  setup do
    @geofencing = geofencings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:geofencings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create geofencing" do
    assert_difference('Geofencing.count') do
      post :create, geofencing: { area: @geofencing.area, entrance_id: @geofencing.entrance_id, nombre: @geofencing.nombre }
    end

    assert_redirected_to geofencing_path(assigns(:geofencing))
  end

  test "should show geofencing" do
    get :show, id: @geofencing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @geofencing
    assert_response :success
  end

  test "should update geofencing" do
    patch :update, id: @geofencing, geofencing: { area: @geofencing.area, entrance_id: @geofencing.entrance_id, nombre: @geofencing.nombre }
    assert_redirected_to geofencing_path(assigns(:geofencing))
  end

  test "should destroy geofencing" do
    assert_difference('Geofencing.count', -1) do
      delete :destroy, id: @geofencing
    end

    assert_redirected_to geofencings_path
  end
end
