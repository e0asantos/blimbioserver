require 'test_helper'

class CampaignsControllerTest < ActionController::TestCase
  setup do
    @campaign = campaigns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:campaigns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create campaign" do
    assert_difference('Campaign.count') do
      post :create, campaign: { descripcion: @campaign.descripcion, entrances_id: @campaign.entrances_id, imagen: @campaign.imagen, impresiones_hechas: @campaign.impresiones_hechas, impresiones_meta: @campaign.impresiones_meta, impresiones_vistas: @campaign.impresiones_vistas, justonce: @campaign.justonce, nombre: @campaign.nombre, tiempo_permanencia: @campaign.tiempo_permanencia }
    end

    assert_redirected_to campaign_path(assigns(:campaign))
  end

  test "should show campaign" do
    get :show, id: @campaign
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @campaign
    assert_response :success
  end

  test "should update campaign" do
    patch :update, id: @campaign, campaign: { descripcion: @campaign.descripcion, entrances_id: @campaign.entrances_id, imagen: @campaign.imagen, impresiones_hechas: @campaign.impresiones_hechas, impresiones_meta: @campaign.impresiones_meta, impresiones_vistas: @campaign.impresiones_vistas, justonce: @campaign.justonce, nombre: @campaign.nombre, tiempo_permanencia: @campaign.tiempo_permanencia }
    assert_redirected_to campaign_path(assigns(:campaign))
  end

  test "should destroy campaign" do
    assert_difference('Campaign.count', -1) do
      delete :destroy, id: @campaign
    end

    assert_redirected_to campaigns_path
  end
end
