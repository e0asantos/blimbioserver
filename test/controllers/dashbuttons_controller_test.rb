require 'test_helper'

class DashbuttonsControllerTest < ActionController::TestCase
  setup do
    @dashbutton = dashbuttons(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dashbuttons)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dashbutton" do
    assert_difference('Dashbutton.count') do
      post :create, dashbutton: { id_owner: @dashbutton.id_owner, id_servicio: @dashbutton.id_servicio }
    end

    assert_redirected_to dashbutton_path(assigns(:dashbutton))
  end

  test "should show dashbutton" do
    get :show, id: @dashbutton
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dashbutton
    assert_response :success
  end

  test "should update dashbutton" do
    patch :update, id: @dashbutton, dashbutton: { id_owner: @dashbutton.id_owner, id_servicio: @dashbutton.id_servicio }
    assert_redirected_to dashbutton_path(assigns(:dashbutton))
  end

  test "should destroy dashbutton" do
    assert_difference('Dashbutton.count', -1) do
      delete :destroy, id: @dashbutton
    end

    assert_redirected_to dashbuttons_path
  end
end
