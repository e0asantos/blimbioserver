require 'test_helper'

class IopushesControllerTest < ActionController::TestCase
  setup do
    @iopush = iopushes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:iopushes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create iopush" do
    assert_difference('Iopush.count') do
      post :create, iopush: { device_id: @iopush.device_id, token: @iopush.token, user_id: @iopush.user_id }
    end

    assert_redirected_to iopush_path(assigns(:iopush))
  end

  test "should show iopush" do
    get :show, id: @iopush
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @iopush
    assert_response :success
  end

  test "should update iopush" do
    patch :update, id: @iopush, iopush: { device_id: @iopush.device_id, token: @iopush.token, user_id: @iopush.user_id }
    assert_redirected_to iopush_path(assigns(:iopush))
  end

  test "should destroy iopush" do
    assert_difference('Iopush.count', -1) do
      delete :destroy, id: @iopush
    end

    assert_redirected_to iopushes_path
  end
end
