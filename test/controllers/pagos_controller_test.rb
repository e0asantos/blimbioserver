require 'test_helper'

class PagosControllerTest < ActionController::TestCase
  setup do
    @pago = pagos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pagos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pago" do
    assert_difference('Pago.count') do
      post :create, pago: { campaign_id: @pago.campaign_id, cantidad_final: @pago.cantidad_final, cantidad_original: @pago.cantidad_original, estado_payment: @pago.estado_payment, fechafin: @pago.fechafin, fechainicio: @pago.fechainicio, usuarios_id: @pago.usuarios_id }
    end

    assert_redirected_to pago_path(assigns(:pago))
  end

  test "should show pago" do
    get :show, id: @pago
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pago
    assert_response :success
  end

  test "should update pago" do
    patch :update, id: @pago, pago: { campaign_id: @pago.campaign_id, cantidad_final: @pago.cantidad_final, cantidad_original: @pago.cantidad_original, estado_payment: @pago.estado_payment, fechafin: @pago.fechafin, fechainicio: @pago.fechainicio, usuarios_id: @pago.usuarios_id }
    assert_redirected_to pago_path(assigns(:pago))
  end

  test "should destroy pago" do
    assert_difference('Pago.count', -1) do
      delete :destroy, id: @pago
    end

    assert_redirected_to pagos_path
  end
end
