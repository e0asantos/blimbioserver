require 'test_helper'

class AnalyticsControllerTest < ActionController::TestCase
  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get logs" do
    get :logs
    assert_response :success
  end

  test "should get chart" do
    get :chart
    assert_response :success
  end

end
