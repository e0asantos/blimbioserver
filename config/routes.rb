Rails.application.routes.draw do

  match '/push/history',:controller=>'iopushes',:action=>'generateHistory',via:[:get]
  match '/push/update',:controller=>'iopushes',:action=>'updateToken',via:[:post]
  resources :iopushes
  
  resources :plastics

  resources :likes

  resources :notifications

  resources :pushes

  resources :adserviceusers

  match '/adservices/getAdByPlace',:controller=>'adservices',:action=>'getAdByPlace',via:[:get,:post]
  resources :adservices


  match '/dashbuttons/attachDash',:controller=>'dashbuttons',:action=>'attachDash',via:[:get,:post]
  match '/dashbuttons/pushDash',:controller=>'dashbuttons',:action=>'pushDash',via:[:get,:post]
  resources :dashbuttons

  match '/servicios/beacon',:controller=>'servicios',:action=>'showServicesByLocation',via:[:get,:post]
  resources :servicios

  match '/mastercard/yomero',:controller=>'mastercards',:action=>'yomero',via:[:get,:post]
  match '/mastercard/fromBeacon',:controller=>'mastercards',:action=>'fromBeacon',via:[:get,:post]
  match '/mastercard/closeTransaction',:controller=>'mastercards',:action=>'closeTransaction',via:[:get,:post]
  resources :mastercards

  resources :publicidads

  get 'analytics/show'

  get 'analytics/logs'

  get 'analytics/chart'

  match '/analytics/destroy',:controller=>'analytics',:action=>'destroy',via: [:post]

  match '/pagos/calculatePrice',:controller=>'pagos',:action=>'calculatePrice',via: [:get, :post]
  match '/pagos/incomingFromGateway',:controller=>'pagos',:action=>'incomingFromGateway',via: [:get, :post]


  match '/pagos/fake',:controller=>'pagos',:action=>'fakeAuthorize',via: [:get]
  resources :pagos

  resources :preciohoras
  resources :precios

  match '/campaigns/getRankedCampaigns',:controller=>'campaigns',:action=>'getRankedCampaigns',via: [:get,:post]
  match '/campaigns/buy',:controller=>'campaigns',:action=>'commitToBuy',via: [:post]
  resources :campaigns

  resources :geofencings


  
  resources :places

  resources :welcomes

  resources :mail_templates

  match '/entrances/getAllUsersAsJson',:controller=>'entrances',:action=>'getAllUsersAsJson',via: [:get, :post]
  match '/entrances/getUserPromos',:controller=>'entrances',:action=>'getUserPromos',via: [:get, :post]
  match '/entrances/logout',:controller=>'entrances',:action=>'logout',via: [:get, :post]
  match '/entrances/createToken',:controller=>'entrances',:action=>'createToken',via: [:get,:post]
  match '/entrances/getUserDetails',:controller=>'entrances',:action=>'getUserDetails',via: [:get,:post]
  match '/entrances/setUserDetails',:controller=>'entrances',:action=>'setUserDetails',via: [:get,:post]
  match '/entrances/history',:controller=>'entrances',:action=>'history',via: [:get,:post]
  match '/reservations/getplaces',:controller=>'entrances',:action=>'getParkingLot',via: [:post]

  resources :entrances
  get '/login', to: 'entrances#index'
  get '/register', to: 'entrances#index'
  get '/forget', to: 'entrances#index'


  match '/entrances/createAccount',:controller=>'entrances',:action=>'createAccount',via: [:get, :post]
  match '/entrances/resetAccount',:controller=>'entrances',:action=>'resetAccount',via: [:get, :post]
  get '/entrances/confirmAccount/:id',:controller=>'entrances',:action=>'confirmAccount'
  match '/entrances/loginAccount',:controller=>'entrances',:action=>'loginAccount',via: [:get, :post]


  match '/beacons/getCloudBeacons',:controller=>'beacons',:action=>'getCloudBeacons',via: [:get,:post]
  match '/beacons/interchangeBeaconLocation',:controller=>'beacons',:action=>'interchangeBeaconLocation',via: [:get,:post]
  match '/beacons/exchangeReward',:controller=>'beacons',:action=>'exchangeReward',via: [:get,:post]
  match '/beacons/isRewardAvailable',:controller=>'beacons',:action=>'isRewardAvailable',via: [:get,:post]
  match '/beacons/getOliiPrice',:controller=>'beacons',:action=>'getOliiPrice',via: [:get,:post]
  match '/beacons/getPaidOlii',:controller=>'beacons',:action=>'getPaidOlii',via: [:get,:post]
  resources :beacons

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
