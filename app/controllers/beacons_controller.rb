#encoding: utf-8
require 'pusher'
include ActionView::Helpers::TextHelper
class BeaconsController < ApplicationController
  before_action :set_beacon, only: [:show, :edit, :update, :destroy]
  protect_from_forgery except: :interchangeBeaconLocation

  def getCloudBeacons
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    beacons=[]
    # buscar todos los beacons y enviarlos al cliente
    Beacon.where(:activo=>1).each do |singleBeacon|
      unBeacon=Hash.new
      unBeacon[:UUID]=singleBeacon.uid
      unBeacon[:lat]=singleBeacon.latitud
      unBeacon[:lon]=singleBeacon.longitud
      unBeacon[:minor]=singleBeacon.uid_low
      unBeacon[:major]=singleBeacon.uid_high
      unBeacon[:nombre]=singleBeacon.nombre
      beacons.push(unBeacon)
    end
    beacons=beacons.uniq
    respond_to do |format|
        format.json { render json: beacons.to_json  }
      end
  end

  def getOliiPrice
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end

    # buscamos una entrada que tenga el precio
    precio=Precio.where(:olii=>1)

    respond_to do |format|
        format.json { render json: precio.to_json  }
      end
  end

  def getPaidOlii
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end

    # buscamos una entrada que tenga el precio
    oliis=Pago.where(:olii=>1,:estado_payment=>"charge.succeeded",:usuarios_id=>session[:cuser].id)

    respond_to do |format|
        format.json { render json: oliis.to_json  }
      end
  end

  def exchangeReward
     if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    entradaPrev=Beaconlog.find_by_id(params[:rewid].to_i)
    puts "ENTRADA_PREV:"+entradaPrev.inspect
    verificarEntradas=Beaconlog.where(:uidevice=>params[:cdevice],:beacons_id=>entradaPrev.beacons_id,:campaing_id=>entradaPrev.campaing_id).index_by(&:client_id).keys
    puts "ENTRADA_PREV:"+verificarEntradas.inspect
    # aqui hay q verificar si dentro de los usuarios esta este usuario, si no esta, es un usuario que esta usando el mismo telefono
    entrada=nil
    if verificarEntradas.include?(Entrance.find(session[:cuser]).id) or verificarEntradas.length==0
      puts "SUPUESTO USUARIO UNICO"
      entrada=entradaPrev
    end
    # con id de reward se puede canjear
    resultado="ERROR"
     if entrada!=nil
      if (entrada.activo==nil or entrada.activo=="") and (entrada.uidevice==nil)
        # aqui comprobamos el codigo de desactivacion
        ttcampana=Campaign.find_by_id(entrada.campaing_id)
        if ttcampana.desactivacion==params[:deactivate].strip
          entrada.activo=params[:deactivate].strip
          entrada.uidevice=params[:cdevice]
          entrada.rewardeddate=DateTime.now
          entrada.save
          resultado="OK"
        end
      end
    end
    Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
    respond_to do |format|
      puts resultado.inspect
        format.json { render json: resultado.to_json  }
    end
  end

  def isRewardAvailable
     if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    # PAID
    resultado="PAID"
    entrada=Beaconlog.find_by_id(params[:rewid].to_i)
    if entrada!=nil
      if entrada.activo==nil or entrada.activo==""
        resultado="CONTINUE"
      end
    end
    respond_to do |format|
        format.json { render json: resultado.to_json  }
      end
  end

  def interchangeBeaconLocation
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    # aqui recibimos las lecturas de beacons y los buscamos
    # formato  33334444555556666:123123:123123
    formato=params[:fullUUID].split(":")
    # hay que destacar que el beacon puede tener campaigns entonces primero buscamos el beacon
    beacon=Beacon.where(:uid=>formato[0].downcase,:uid_low=>formato[1],:uid_high=>formato[2]).last
    allCampaigns=[]
    internalFlux=""
    if params[:flux]=="incoming"
      internalFlux="onexit!=1"
    else
      internalFlux="onexit=1"
    end
    if beacon!=nil
      historial=BeaconHistory.new
      historial.entraces_id=Entrance.find(session[:cuser]).id
      historial.beacons_id=beacon.id
      historial.flux=params[:flux]
      historial.save
      #buscamos los pagos donde se encuentre el beacon
      pagosExtracted=Pago.where(:beacons_id=>beacon.id)
      # hay que obtener las campañas de entrada o salida
      pagos=[]
      pagosExtracted.each do |singlePagoExtracted|
        extractedCamp=Campaign.find_by_id(singlePagoExtracted.campaign_id)
        if extractedCamp!=nil
          if extractedCamp.onexit==1 and params[:flux]=="outgoing" and extractedCamp.parkinglot!=1
            pagos.push(singlePagoExtracted)
          elsif extractedCamp.onexit!=1 and params[:flux]=="incoming"  and extractedCamp.parkinglot!=1
            pagos.push(singlePagoExtracted)
          end
            
        end
      end
      posiblesCupones=[]
      pagos.each do |singlePago|
        # si el pago no tiene fecha de inicio ni de fin, entonces hay que verificar si tiene aun credito para enviar un mensaje a esta hora
        # hay que verificar si el beacon tiene precios por horario
        puts "--"
        puts singlePago.fechainicio
        puts "--"
        if singlePago.fechainicio==nil and singlePago.fechafin==nil
          puts "FECHA INICIO VACIA"
          priceNow=Precio.find_by_id(beacon.precios_id).precioperpush.to_f
          preciosHoras=Preciohora.where(:plan=>beacon.precios_id).where('hora<=?',Time.now.strftime("%l")).where('horafin>=?',Time.now.strftime("%l")).last
          if preciosHoras!=nil
            priceNow=preciosHoras.precio.to_f
            # es variable en la hora, veamos si la hora es just en este momento
          end
          puts singlePago.credito.inspect+"___"+priceNow.inspect
          if singlePago.credito.to_f>priceNow
            # ademas hay que saber si el cupon solo debe llegar una sola vez
            posiblementeSI=-1
            if Campaign.find_by_id(singlePago.campaign_id).justonce==1 and Beaconlog.where(:campaing_id=>singlePago.campaign_id).count==0
              posiblementeSI=singlePago.id
              # posiblesCupones.push(singlePago.id)
            elsif Campaign.find_by_id(singlePago.campaign_id).justonce!=1
              posiblementeSI=singlePago.id
              # posiblesCupones.push(singlePago.id)
            end
            
            if posiblementeSI>-1
              # hay que verificar el resto de las condiciones de sexo, maximo numero de envios, fecha etc
              # revisar si ya alcanzo los maximos
              cobrados=Beaconlog.where(:campaing_id=>Pago.find_by_id(posiblementeSI).campaign_id).where("rewardeddate is not null")
              thisCamp=Campaign.find_by_id(Pago.find_by_id(posiblementeSI).campaign_id)
              if (thisCamp.stoprewardsdate>Date.today or thisCamp.stoprewardsdate==nil) and (thisCamp.stoprewardsat>cobrados.length or thisCamp.stoprewardsat==0)
                # ahora verificar edades y sexo
                # 1 es hombre
                # 2 es mujer
                # 0 es todos
                if thisCamp.sex==0 and age(Entrance.find(session[:cuser]).birthday)>=thisCamp.agefrom and age(Entrance.find(session[:cuser]).birthday)<=thisCamp.ageto
                  posiblesCupones.push(posiblementeSI)
                elsif Entrance.find(session[:cuser]).sexo=="H" and thisCamp.sex==1 and age(Entrance.find(session[:cuser]).birthday)>=thisCamp.agefrom and age(Entrance.find(session[:cuser]).birthday)<=thisCamp.ageto
                  posiblesCupones.push(posiblementeSI)
                elsif Entrance.find(session[:cuser]).sexo=="M" and thisCamp.sex==2 and age(Entrance.find(session[:cuser]).birthday)>=thisCamp.agefrom and age(Entrance.find(session[:cuser]).birthday)<=thisCamp.ageto
                  posiblesCupones.push(posiblementeSI)
                end
                  
              end
            end
              
          end
        else
          puts "HAY FECHA"
          # en caso que tengamos fecha, hay que verificar que estemos dentro de la fecha

          if singlePago.fechafin>=DateTime.now

            # entonces aun esta vigente esta campaña, hay que verificar que en los logs el numero de impresiones no se haya cumplido
            posiblementeSII=-1
            if Beaconlog.where(:campaing_id=>singlePago.campaign_id).count<Campaign.find_by_id(singlePago.campaign_id).impresiones_meta
              if Campaign.find_by_id(singlePago.campaign_id).justonce==1 and Beaconlog.where(:campaing_id=>singlePago.campaign_id).count==0
                # posiblesCupones.push(singlePago.id)
                posiblementeSII=singlePago.id
              elsif Campaign.find_by_id(singlePago.campaign_id).justonce!=1
                # posiblesCupones.push(singlePago.id)
                posiblementeSII=singlePago.id
              end
            end

            if posiblementeSII>-1
              # hay que verificar el resto de las condiciones de sexo, maximo numero de envios, fecha etc
              # revisar si ya alcanzo los maximos
              cobrados=Beaconlog.where(:campaing_id=>Pago.find_by_id(posiblementeSII).campaign_id).where("rewardeddate is not null")
              thisCamp=Campaign.find_by_id(Pago.find_by_id(posiblementeSII).campaign_id)
              if (thisCamp.stoprewardsdate>Date.today or thisCamp.stoprewardsdate==nil) and (thisCamp.stoprewardsat>cobrados.length or thisCamp.stoprewardsat==0)
                # ahora verificar edades y sexo
                if thisCamp.sex==0 and age(Entrance.find(session[:cuser]).birthday)>=thisCamp.agefrom and age(Entrance.find(session[:cuser]).birthday)<=thisCamp.ageto
                  posiblesCupones.push(posiblementeSII)
                elsif Entrance.find(session[:cuser]).sexo=="H" and thisCamp.sex==1 and age(Entrance.find(session[:cuser]).birthday)>=thisCamp.agefrom and age(Entrance.find(session[:cuser]).birthday)<=thisCamp.ageto
                  posiblesCupones.push(posiblementeSII)
                elsif Entrance.find(session[:cuser]).sexo=="M" and thisCamp.sex==2 and age(Entrance.find(session[:cuser]).birthday)>=thisCamp.agefrom and age(Entrance.find(session[:cuser]).birthday)<=thisCamp.ageto
                  posiblesCupones.push(posiblementeSII)
                end
                  
              end
            end




          end
        end
        
      end
    
      # antes de obtener el cupon final, escogemos solo uno
      r = Random.new
      finalChoice=-1
      finalChoicePayment=0
      if posiblesCupones.length>1
        finalChoicePayment=Pago.find_by_id(posiblesCupones[r.rand(0...posiblesCupones.length-1)])
        finalChoice=finalChoicePayment.campaign_id
      elsif posiblesCupones.length==1
        finalChoicePayment=Pago.find_by_id(posiblesCupones[0])
        finalChoice=finalChoicePayment.campaign_id
      end
        
      if finalChoice>-1
        # publicamos todas las campañas que se pueden enviar
        fccampaign=Campaign.find_by_id(finalChoice)
        puts "campañas:"+posiblesCupones.to_s
        puts "finalChoice:"+finalChoice.to_s
        puts "Campaign:"+Campaign.find_by_id(finalChoice).inspect
        # antes de enviar la campaña, hay que verificar si la campaña es unica del beacon del usuario,
        # en caso de que la campaña solo pueda enviarse por ese beacon, entonces verificamos que se mande la campaña
        # actual mas los que se marcan como bundle
        if Campaign.find_by_id(finalChoice)!=nil
          puts "CREANDO CAMPAÑA UNICA"
          tcampana=Hash.new
          if Campaign.find_by_id(finalChoice).imagen==nil or Campaign.find_by_id(finalChoice).imagen==""
            tcampana[:url]="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text="+Campaign.find_by_id(finalChoice).nombre
          else
            tcampana[:url]=Campaign.find_by_id(finalChoice).imagen
          end
          tcampana[:descripcion]=Campaign.find_by_id(finalChoice).descripcion
          tcampana[:negocio]=Campaign.find_by_id(finalChoice).nombre
          tcampana[:timer]=Campaign.find_by_id(finalChoice).tiempo_permanencia
          tcampana[:popup]=Campaign.find_by_id(finalChoice).cupon
          tcampana[:promo]=Campaign.find_by_id(finalChoice).promocode
          tcampana[:price]=Campaign.find_by_id(finalChoice).price
          tcampana[:beacon]=formato[0]
          tcampana[:id]=finalChoice
          tcampana[:likes]=Like.where(:campaign_id=>finalChoice).length
          tcampana[:llave]=formato[1]+":"+formato[2]
          tcampana[:onexit]=Campaign.find_by_id(finalChoice).onexit
          tcampana[:fechafin]=Campaign.find_by_id(finalChoice).stoprewardsdate
          tcampana[:fechatext]=Campaign.find_by_id(finalChoice).stoprewardsdate.strftime("%b,%a,%d").downcase
          # ahora agregarlo al log
          registroNuevo=Beaconlog.new
          registroNuevo.beacons_id=beacon.id
          registroNuevo.client_id=session[:cuser]
          registroNuevo.campaing_id=Campaign.find_by_id(finalChoice).id
          registroNuevo.save
          tcampana[:reward]=registroNuevo.id
          allCampaigns.push(tcampana)
        end
        puts "BEACON_OWNER:"+beacon.owner.inspect+"     CAMPAÑA:"+fccampaign.entrances_id.inspect
        if beacon.owner==fccampaign.entrances_id and Campaign.find_by_id(finalChoice).bundle==1
          puts "CREANDO BUNDLE"
          allCampaigns=[]
          # la campaña y el dueño del beacon son el mismo, entonces hay q traer las campañas que estaban activas en modo bundle
          posiblesCupones.each do |singleCuponFinal|
            prePayment=Pago.find_by_id(singleCuponFinal)
            puts "BUSCANDO CAMPAÑA:"+singleCuponFinal.to_s
            preCampaign=Campaign.find_by_id(prePayment.campaign_id)
            if preCampaign!=nil
              if preCampaign.bundle==1
                tcampanab=Hash.new
                if preCampaign.imagen==nil or preCampaign.imagen==""
                  tcampanab[:url]="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=Cupon"
                else 
                  tcampanab[:url]="http://panel.blimb.io/campaigni/"+preCampaign.imagen
                end
                tcampanab[:descripcion]=preCampaign.descripcion
                tcampanab[:negocio]=preCampaign.nombre
                tcampanab[:timer]=preCampaign.tiempo_permanencia
                tcampanab[:popup]=preCampaign.cupon
                tcampanab[:reward]=registroNuevo.id
                tcampanab[:promo]=preCampaign.promocode
                tcampanab[:beacon]=formato[0]
                tcampanab[:id]=preCampaign.id
                tcampanab[:likes]=Like.where(:campaign_id=>preCampaign.id).length
                tcampanab[:llave]=formato[1]+":"+formato[2]
                tcampanab[:onexit]=preCampaign.onexit
                tcampanab[:fechafin]=preCampaign.stoprewardsdate
                tcampanab[:fechatext]=preCampaign.stoprewardsdate.strftime("%b,%a,%d").downcase
                allCampaigns.push(tcampanab)
                if preCampaign.stoprewardsdate!=nil
                  if preCampaign.stoprewardsdate>=DateTime.now
                    enTexto=time_diff_in_natural_language(DateTime.now,preCampaign.stoprewardsdate).to_s
                    if enTexto.index("year")!=nil
                      enTexto["year"]="año"  
                    end
                    if enTexto.index("month")!=nil
                    enTexto["month"]="mes"
                    end
                    if enTexto.index("day")!=nil
                    enTexto["day"]="día"
                    end
                    if enTexto.index("hour")!=nil
                    enTexto["hour"]="hora"
                    end
                    if enTexto.index("minute")!=nil
                    enTexto["minute"]="minuto"
                    end
                     if enTexto.index("week")!=nil
                    enTexto["week"]="semana"
                    end
                    
                    tcampanab[:remaining]=enTexto
                    

                  else
                    tcampanab[:remaining]="Promoción vencida"
                  end

                else
                  tcampanab[:remaining]="Sin fecha de caducidad"
                end
                registroNuevo=Beaconlog.new
                registroNuevo.beacons_id=beacon.id
                registroNuevo.client_id=session[:cuser]
                registroNuevo.campaing_id=preCampaign.id
                registroNuevo.save
                
              end
            end
          end
        end
        priceNow=Precio.find_by_id(beacon.precios_id).precioperpush.to_f
        preciosHoras=Preciohora.where(:plan=>beacon.precios_id).where('hora<=?',Time.now.strftime("%l")).where('horafin>=?',Time.now.strftime("%l")).last
        if preciosHoras!=nil
          priceNow=preciosHoras.precio.to_f
          # es variable en la hora, veamos si la hora es just en este momento
        end
        finalChoicePayment.credito=finalChoicePayment.credito.to_f-priceNow
        finalChoicePayment.save
        
      end
      
    end
    Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
    Pusher.trigger("incuvaSite", "dashboard", {
      message: "New campaign"
    })
    puts "regresa:"+allCampaigns.to_json
    respond_to do |format|
        format.json { render json: allCampaigns.to_json  }
    end
  end

  def beaconTester
    
  end

  def age(dob)
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end
  # GET /beacons
  # GET /beacons.json
  def index
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    if Entrance.find_by_id(session[:cuser]).superlevel<1000
      redirect_to "/"
      return
    end
    @beacons = Beacon.all
  end

  # GET /beacons/1
  # GET /beacons/1.json
  def show
  end

  # GET /beacons/new
  def new
    @beacon = Beacon.new
    @beacons=[]

  end

  # GET /beacons/1/edit
  def edit
    
  end

  # POST /beacons
  # POST /beacons.json
  def create
    @beacon = Beacon.new(beacon_params)

    respond_to do |format|
      if @beacon.save
        Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
        format.html { redirect_to "/beacons", notice: 'Beacon was successfully created.' }
        format.json { render :show, status: :created, location: @beacon }
      else
        format.html { render :new }
        format.json { render json: @beacon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /beacons/1
  # PATCH/PUT /beacons/1.json
  def update
    respond_to do |format|
      if @beacon.update(beacon_params)
        Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
        format.html { redirect_to "/beacons", notice: 'Beacon was successfully updated.' }
        format.json { render :show, status: :ok, location: @beacon }
      else
        format.html { render :edit }
        format.json { render json: @beacon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /beacons/1
  # DELETE /beacons/1.json
  def destroy
    @beacon.destroy
    Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
    respond_to do |format|
      format.html { redirect_to beacons_url, notice: 'Beacon was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def time_diff_in_natural_language(from_time, to_time)
    from_time = from_time.to_time if from_time.respond_to?(:to_time)
    to_time = to_time.to_time if to_time.respond_to?(:to_time)
    distance_in_seconds = ((to_time - from_time).abs).round
    components = []

    %w(year month week day hour minute).each do |interval|
      # For each interval type, if the amount of time remaining is greater than
      # one unit, calculate how many units fit into the remaining time.
      if distance_in_seconds >= 1.send(interval)
        delta = (distance_in_seconds / 1.send(interval)).floor
        distance_in_seconds -= delta.send(interval)
        components << pluralize(delta, interval)
      end
    end

    components.join(", ")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beacon
      @beacon = Beacon.find(params[:id])
      @beacons=[]
      @beacons.push(@beacon)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def beacon_params
      params.require(:beacon).permit(:id, :nombre, :latitud, :longitud, :radio, :uid, :uid_low, :uid_high,:descripcion,:ciudad,:activo,:owner,:precios_id,:ijjoapp)
    end
end
