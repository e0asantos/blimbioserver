#encoding: utf-8
require 'openpay'
require 'securerandom'
class PagosController < ApplicationController
  protect_from_forgery except: :incomingFromGateway
  before_action :set_pago, only: [:show, :edit, :update, :destroy]

  def incomingFromGateway

    if params[:type]=="verification"
      verif=Pago.new
      verif.paymentuid=params[:verification_code]
      verif.estado_payment=params[:verification_code]
      verif.save
      render :nothing => true, :status => 200
      return
    end
    puts "TRANSACTION=>"+params[:transaction][:id]
    puts "TYPE=>"+params[:type]
      supuestoPago=Pago.where("paymentuid='"+params[:transaction][:id]+"' or paymentuid='"+params[:transaction][:order_id]+"'")
      supuestoPago.each do |singlePago|
        if singlePago.estado_payment!=params[:type]
        singlePago.estado_payment=params[:type]
        singlePago.save
        campaign=Campaign.find_by_id(singlePago.campaign_product)
        if params[:type]=="charge.succeeded"
          orderPlaced(singlePago.usuarios_id,"Pago por $"+singlePago.cantidad_final.to_s+" de "+campaign.nombre+" se ha procesado correctamente.")  
        else
          orderPlaced(singlePago.usuarios_id,"Pago por $"+singlePago.cantidad_final.to_s+" de "+campaign.nombre+" no se ha podido procesar.")  
        end
        

         # >>> 7 julio 2016, se agrega un cupon virtual
         if singlePago.campaign_product!=nil

           
           orderPlaced(campaign.entrances_id,"Pago por $"+singlePago.cantidad_final.to_s+" de "+campaign.nombre+" recibido.")  
           newFakePromo=Beaconlog.new
           newFakePromo.client_id=singlePago.usuarios_id
           newFakePromo.campaing_id=singlePago.campaign_product
           newFakePromo.is_product=1
           # calcular la fecha de fin de disponibilidad
           if campaign.duration.minutes!=nil
            if campaign.duration.minutes>0
              newFakePromo.end_date=Time.now+campaign.duration.minutes  
            end
           end
           
           newFakePromo.save
           singlePago.beaconlog_id=newFakePromo.id
           singlePago.save
         end
        
        # <<<<<

        # >>>>enviar notificacion si es estacionamiento
        if campaign.parkinglot==1
          campaign=Campaign.find_by_id(singlePago.campaign_product)
          orderPlaced(campaign.entrances_id,"Reserva realizada con pago por $"+supuestoPago.cantidad_final.to_s+" de "+campaign.nombre+" recibido.")  
        end
        # <<<<<
      end
     end  
    
    
   
    


    render :nothing => true, :status => 200
  end

  def orderPlaced(userid,msg)
    # buscar los tokens del usuario
    tokens=Iopush.where(:user_id=>userid).index_by(&:token).keys
    mobileNotification(tokens,msg)
  end

  def fakeAuthorize
    supuestoPago=Pago.where(:paymentuid=>params[:transaction])
    supuestoPago.each do |singlePago|
    singlePago.estado_payment="charge.succeeded"
    singlePago.save

     if params[:type]=="charge.succeeded"
        orderPlaced(supuestoPago.usuarios_id,"Pago por $"+supuestoPago.cantidad_final.to_s+" de "+thisProduct.nombre+" se ha procesado correctamente.")  
      else
        orderPlaced(supuestoPago.usuarios_id,"Pago por $"+supuestoPago.cantidad_final.to_s+" de "+thisProduct.nombre+" no se ha podido procesar.")  
      end
      

       # >>> 7 julio 2016, se agrega un cupon virtual
       if singlePago.campaign_product!=nil

         campaign=Campaign.find_by_id(singlePago.campaign_product)
         orderPlaced(campaign.entrances_id,"Pago por $"+supuestoPago.cantidad_final.to_s+" de "+thisProduct.nombre+" recibido.")  
         newFakePromo=Beaconlog.new
         newFakePromo.client_id=singlePago.usuarios_id
         newFakePromo.campaing_id=singlePago.campaign_product
         newFakePromo.is_product=1
         # calcular la fecha de fin de disponibilidad
         if campaign.duration.minutes!=nil
          if campaign.duration.minutes>0
            newFakePromo.end_date=Time.now+campaign.duration.minutes  
          end
         end
         
         newFakePromo.save
         singlePago.beaconlog_id=newFakePromo.id
         singlePago.save
       end
      
      # <<<<<

      # >>>>enviar notificacion si es estacionamiento
      if singlePago.parkinglot==1
        campaign=Campaign.find_by_id(singlePago.campaign_product)
        orderPlaced(campaign.entrances_id,"Reserva realizada con pago por $"+supuestoPago.cantidad_final.to_s+" de "+thisProduct.nombre+" recibido.")  
      end
      # <<<<<
     end  
    render :nothing => true, :status => 200
  end

  def calculatePrice
    
    # primero obtenemos los beacons
    selectedBeacons=params[:beacons].split(",")
    campaignid=params[:campaign].to_i
    precio=calculatePriceInternal(selectedBeacons,campaignid,nil)
    
    respond_to do |format|
        format.json { render json: precio.to_json }
    end
  end

  def calculatePriceInternal(selectedBeacons,campaignid,priceForTheBeaconID)
    puts "calculatePriceInternal:"+selectedBeacons.inspect+":::"+campaignid.inspect+"::::"+priceForTheBeaconID.inspect
    precio=Hash.new
    precio[:precio]="Esta campaña no procede"
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    campana=nil
    if currentUser.superlevel>=1000
      campana=Campaign.where(:id=>campaignid).last
    else
      campana=Campaign.where(:id=>campaignid,:entrances_id=>session[:cuser].to_i).last
    end
    # if campana==nil
    #   return precio          ---->si puede ser nil cuando solo se pago por el beacon, no tiene campaña
    # end
    precio[:precio]=0
    # buscamos que los beacons sean del usuario o de consultware
    selectedBeacons.each do |singleBeacon|
      beaconRecord=Beacon.where(:id=>singleBeacon.to_i).where("owner is NULL or owner="+session[:cuser].to_s).last
      if beaconRecord!=nil
        # necesitamos determinar un promedio de los mensajes en cada beacon
        impresiones=0
        if campana!=nil
          impresiones=(campana.impresiones_meta/selectedBeacons.length).ceil
        end
        # si puede ver este beacon, entonces hay que revisar los precios, el precio lo vamos a calcular por promedio de los precios por hora
        # consultando precios por hora
        precioDeBeacon=0
        if Precio.where(:id=>beaconRecord.precios_id).last.precio.to_f>0
          # no hace falta buscar el precio por push por que el usuario esta adquiriendo un plan
          precioDeBeacon=Precio.where(:id=>beaconRecord.precios_id).last.precio.to_f
          impresiones=-1
        else
          precioDeBeacon=Precio.where(:id=>beaconRecord.precios_id).last.precioperpush.to_f
        end
        
        precioPromedio=0
        if Preciohora.where(:plan=>beaconRecord.precios_id).length>0 and impresiones>0
          precioPromedio=Preciohora.where(:plan=>beaconRecord.precios_id).sum(:precio)/Preciohora.where(:plan=>beaconRecord.precios_id).length
        end
        puts "PROMEDIO:"+precioPromedio.to_s
        if precioPromedio>0
          # tenemos precios variantes
          precioDeBeacon=precioPromedio
        end
        # es importante revisar si el beacon tiene propietario, si el propietario es la persona que esta haciendo el precio entonces revisar si ya lo tiene pagado
        # en tal caso entonces no lleva precio
        puts "BEACON OWNER:"+beaconRecord.owner.inspect
        puts "CURRENT USER:"+currentUser.id.inspect
        if beaconRecord.owner==currentUser.id
          # revisamos si esta pagado
          puts "MISMO OWNER"
          if Pago.where(:beacons_id=>beaconRecord.id,:usuarios_id=>currentUser.id).where('fechafin>=?',DateTime.now).length>0
            # entonces si ha pagado y sigue activo, por lo que vamos a poner cero al precio
            precioDeBeacon=0
          end
        end
        if impresiones==-1
          impresiones=1
        end
        # ahora multiplicamos el precio del beacon por los mensajes que le tocarian y lo sumamos al precio final
        if priceForTheBeaconID==nil
          precio[:precio]=precio[:precio]+(precioDeBeacon*impresiones)  
        elsif priceForTheBeaconID.to_i==singleBeacon.to_i
          precio[:precio]=precio[:precio]+(precioDeBeacon*impresiones)
        end
        
      end
    end
    return precio
  end
  # GET /pagos
  # GET /pagos.json
  def index
    if session[:cuser]==nil
      respond_to do |format|
          format.html { redirect_to "/login" }
      end
    end
    currentUser=Entrance.find_by_id(session[:cuser])
    if currentUser.superlevel>=1000
      @pagos = Pago.all
    else
      @pagos = Pago.where(:usuarios_id=>session[:cuser])
    end
    
  
    
  end

  # GET /pagos/1
  # GET /pagos/1.json
  def show
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
  end

  # GET /pagos/new
  def new
    @pago = Pago.new
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
  end

  # GET /pagos/1/edit
  def edit
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
  end

  # POST /pagos
  # POST /pagos.json
  def create
    @pago = Pago.new(pago_params)
    currentUserC2=Entrance.where(:id=>session[:cuser].to_i).last
    selectedBeacons=[]
    campaignid=nil
    if params.has_key?(:beacons_array)
      selectedBeacons=params[:beacons_array].split(",")
      campaignid=params[:beacons_campaign].to_i
    else
      # es un pago generado de forma normal
      selectedBeacons.push(params[:beacons][:beacons_id].to_i)
      if params[:beacons][:campaign_id]!="none"
        campaignid=params[:beacons][:campaign_id].to_i
      end
    end
    if selectedBeacons.length==0
      redirect_to "/pagos", :notice=>"La orden no se puede procesar, no se seleccionaron zonas de marketing de distribución. "
      return
    end
    
    precioGenerado=calculatePriceInternal(selectedBeacons,campaignid,nil)[:precio]
    puts "PRECIO GENERADO:"+precioGenerado.to_s
    respond_to do |format|
      final_charge=nil
      if @pago.save
        # hay que checar si el precio generado es 0 y somos admin
        if (precioGenerado==0 and currentUserC2.superlevel>=1000) or currentUserC2.superlevel>=1000
          # entonces ignoramos el pago a openpay
        elsif precioGenerado==0 and Campaign.find_by_id(campaignid).entrances_id==currentUserC2.id and selectedBeacons.length>0

        else
          
          # ahora guardamos el pago
          merchant_id="muen0ypxkc2xgsf6j8sq"
          private_key="sk_29d9ae2c53764ac4903b19997d6100be"
          openpay=OpenpayApi.new(merchant_id,private_key)
          cargoAEnviar={:method=> "card",
            :source_id => params[:token_id],
            :amount => precioGenerado.to_s,
            :description=> "Pago publicitaria",
            :order_id=> "bea_"+SecureRandom.hex.to_s} 
            #creamos el objeto de cargo
            puts cargoAEnviar.to_json
            charges=openpay.create(:charges)

          begin
            final_charge=charges.create(cargoAEnviar)
          rescue Exception => e
            puts e.inspect
             redirect_to "/pagos", :notice=>"La tarjeta se rechazó, intente más tarde. "+e.description
              return
          end
        end
      # el primer beacon es el que tiene este pago
      
      
      @pago.campaign_id=campaignid
      selectedBeacons.each do |singleBeacon|
        if singleBeacon!=selectedBeacons[0]
          pagoAsociado=Pago.new
          pagoAsociado.campaign_id=campaignid
          pagoAsociado.beacons_id=singleBeacon.to_i
          pagoAsociado.usuarios_id=@pago.usuarios_id
          if Precio.where(:id=>Beacon.where(:id=>pagoAsociado.beacons_id).last.precios_id).last.precio.to_f>0
            pagoAsociado.fechainicio=DateTime.now
            pagoAsociado.fechafin=DateTime.now+Precio.where(:id=>Beacon.where(:id=>pagoAsociado.beacons_id).last.precios_id).last.dias.days
          else
            pagoAsociado.credito=calculatePriceInternal(selectedBeacons,campaignid,singleBeacon)[:precio]
          end
          pagoAsociado.cantidad_original=calculatePriceInternal(selectedBeacons,campaignid,singleBeacon)[:precio]
          pagoAsociado.estado_payment=final_charge['transaction_type']
          pagoAsociado.paymentuid=final_charge['id']
          pagoAsociado.save
        end
      end
      @pago.beacons_id=selectedBeacons[0].to_i
      # si el beacon tiene un precio fijo entonces calcular los dias
      if Precio.where(:id=>Beacon.where(:id=>@pago.beacons_id).last.precios_id).last.precio.to_f>0
        @pago.fechainicio=DateTime.now
        @pago.fechafin=DateTime.now+Precio.where(:id=>Beacon.where(:id=>@pago.beacons_id).last.precios_id).last.dias.days
      else 
        @pago.credito=calculatePriceInternal(selectedBeacons,campaignid,selectedBeacons[0])[:precio]
        @pago.cantidad_original=calculatePriceInternal(selectedBeacons,campaignid,selectedBeacons[0])[:precio]
      end
      if final_charge!=nil
        @pago.paymentuid=final_charge['id']
        @pago.estado_payment=final_charge['transaction_type']  
      elsif precioGenerado==0 and currentUserC2.superlevel>=1000
        @pago.estado_payment="charge.succeeded"
      elsif precioGenerado==0 and Campaign.find_by_id(campaignid).entrances_id==currentUserC2.id and selectedBeacons.length>0
        @pago.estado_payment="charge.succeeded"
      end
      @pago.save
      
      
        puts final_charge
        format.html { redirect_to "/pagos", notice: 'El pago se esta procesando, en breve uno de nustros representantes se pondrá en contacto.' }
        format.json { render :show, status: :created, location: @pago }
      else
        format.html { render :new }
        format.json { render json: @pago.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pagos/1
  # PATCH/PUT /pagos/1.json
  def update
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
    respond_to do |format|
      if @pago.update(pago_params)
        format.html { redirect_to "/pagos", notice: 'Pago se actualizó.' }
        format.json { render :show, status: :ok, location: @pago }
      else
        format.html { render :edit }
        format.json { render json: @pago.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pagos/1
  # DELETE /pagos/1.json
  def destroy
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
    @pago.destroy
    respond_to do |format|
      format.html { redirect_to pagos_url, notice: 'Pago was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pago
      @pago = Pago.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pago_params
      params.require(:pago).permit(:fechainicio, :fechafin, :usuarios_id, :estado_payment, :cantidad_original, :cantidad_final, :campaign_id,:beacons_id,:paymentuid,:credito,:oxxo,:seveneleven,:olii,:guide)
    end
end
