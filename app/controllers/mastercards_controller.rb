require 'socket' # Provides TCPServer and TCPSocket classes
require 'net/http'
require 'net/https'

class MastercardsController < ApplicationController
  before_action :set_mastercard, only: [:show, :edit, :update, :destroy]

  # GET /mastercards
  # GET /mastercards.json
  def index
    @mastercards = Mastercard.all
  end

  # GET /mastercards/1
  # GET /mastercards/1.json
  def show
  end

  def closeTransaction
    # 
    servicio=Mastercard.where(:servicio_id=>params[:servicio]).last
    # transportamos dinero de la cuenta maestra a la cuenta del que atendio
    beneficiario=Entrance.where(:id=>servicio.id_user).last.confirmation
    pediche=Entrance.where(:id=>Servicio.where(:id=>servicio.servicio_id).last.owner_id).last.confirmation
    puts "--------"
    puts beneficiario
    puts pediche
    systemCard="5184680430000030"
    puts "--------"

    cashTransaction=Servicio.where(:id=>servicio.servicio_id).last

    transaccion=sprintf('%019d', rand(17**17))
    transaccion=transaccion[0..18]
    # transaccion="9144510001324131299"
    puts transaccion


    uri = URI.parse "http://dmartin.org:8001/moneysend/v2/transfer?Format=XML"
    request = Net::HTTP::Post.new uri.path
    t = Time.now                        
    request.body = '<?xml version="1.0" encoding="UTF-8"?>
      <TransferRequest>
         <LocalDate>'+t.strftime("%m%d")+'</LocalDate>
         <LocalTime>'+t.strftime("%H%M%S")+'</LocalTime>
         <TransactionReference>'+transaccion+'</TransactionReference>

         <SenderName>CosoApp</SenderName>
         <SenderAddress>
            <Line1>Mexico City</Line1>

            <City>Mexico City</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>78437</PostalCode>
            <Country>MEX</Country>
         </SenderAddress>
         <FundingCard>
            <AccountNumber>'+systemCard+'</AccountNumber>
            <ExpiryMonth>11</ExpiryMonth>
            <ExpiryYear>2016</ExpiryYear>
         </FundingCard>

         <FundingMasterCardAssignedId>123456</FundingMasterCardAssignedId>
         <FundingAmount>
            <Value>'+cashTransaction.price.to_s+'</Value>
            <Currency>484</Currency>
         </FundingAmount>
         <ReceiverName>'+Entrance.where(:id=>servicio.id_user).last.nombre+'</ReceiverName>
         <ReceiverAddress>
            <Line1>Coso</Line1>
            <Line2>PO BOX 12</Line2>
            <City>Mexico DF</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>79906</PostalCode>
            <Country>MEX</Country>
         </ReceiverAddress>
         <ReceiverPhone>8180829367</ReceiverPhone>
         <ReceivingCard>
            <AccountNumber>'+beneficiario+'</AccountNumber>
         </ReceivingCard>
         <ReceivingAmount>
            <Value>182206</Value>
            <Currency>484</Currency>
         </ReceivingAmount>
         <Channel>W</Channel>
         <UCAFSupport>true</UCAFSupport>
         <ICA>009674</ICA>
         <ProcessorId>9000000442</ProcessorId>
         <RoutingAndTransitNumber>990442082</RoutingAndTransitNumber>
         <CardAcceptor>
            <Name>My Local Bank</Name>
            <City>Saint Louis</City>
            <State>MO</State>
            <PostalCode>63101</PostalCode>
            <Country>USA</Country>
         </CardAcceptor>
        <TransactionDesc>P2P</TransactionDesc>
        <MerchantId>123456</MerchantId>
      </TransferRequest>'
      request.content_type = 'application/xml'
      response = Net::HTTP.new(uri.host, uri.port).start { |http| http.request request }
      puts response.body.inspect

      # despues una transaccion directa del pediche al usuario

      uri = URI.parse "http://dmartin.org:8001/moneysend/v2/transfer?Format=XML"
    request = Net::HTTP::Post.new uri.path
    t = Time.now                        
    request.body = '<?xml version="1.0" encoding="UTF-8"?>
      <TransferRequest>
         <LocalDate>'+t.strftime("%m%d")+'</LocalDate>
         <LocalTime>'+t.strftime("%H%M%S")+'</LocalTime>
         <TransactionReference>'+transaccion+'</TransactionReference>

         <SenderName>'+Entrance.where(:id=>Servicio.where(:id=>servicio.servicio_id).last.owner_id).last.nombre+'</SenderName>
         <SenderAddress>
            <Line1>Mexico City</Line1>

            <City>Mexico City</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>78437</PostalCode>
            <Country>MEX</Country>
         </SenderAddress>
         <FundingCard>
            <AccountNumber>'+pediche+'</AccountNumber>
            <ExpiryMonth>11</ExpiryMonth>
            <ExpiryYear>2016</ExpiryYear>
         </FundingCard>

         <FundingMasterCardAssignedId>123456</FundingMasterCardAssignedId>
         <FundingAmount>
            <Value>'+cashTransaction.price.to_s+'</Value>
            <Currency>484</Currency>
         </FundingAmount>
         <ReceiverName>'+Entrance.where(:id=>servicio.id_user).last.nombre+'</ReceiverName>
         <ReceiverAddress>
            <Line1>Coso</Line1>
            <Line2>PO BOX 12</Line2>
            <City>Mexico DF</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>79906</PostalCode>
            <Country>MEX</Country>
         </ReceiverAddress>
         <ReceiverPhone>8180829367</ReceiverPhone>
         <ReceivingCard>
            <AccountNumber>'+beneficiario+'</AccountNumber>
         </ReceivingCard>
         <ReceivingAmount>
            <Value>182206</Value>
            <Currency>484</Currency>
         </ReceivingAmount>
         <Channel>W</Channel>
         <UCAFSupport>true</UCAFSupport>
         <ICA>009674</ICA>
         <ProcessorId>9000000442</ProcessorId>
         <RoutingAndTransitNumber>990442082</RoutingAndTransitNumber>
         <CardAcceptor>
            <Name>My Local Bank</Name>
            <City>Saint Louis</City>
            <State>MO</State>
            <PostalCode>63101</PostalCode>
            <Country>USA</Country>
         </CardAcceptor>
        <TransactionDesc>P2P</TransactionDesc>
        <MerchantId>123456</MerchantId>
      </TransferRequest>'
      request.content_type = 'application/xml'
      response = Net::HTTP.new(uri.host, uri.port).start { |http| http.request request }
      puts response.body.inspect

      # antes de salir borrar el servicio y su mastercard
      if response.body.to_s.index("Error")==nil
        servicio.destroy
        cashTransaction.destroy  
      end
      

      respond_to do |format|        
        format.json { render json: response.body }
    
    end


  end

  def fromBeacon
    
  if params.has_key?(:myid)
      session[:cuser]=params[:myid]
    end
    servicio=Mastercard.new
    servicio.id_user=session[:cuser]
    servicio.servicio_id=params[:servicio]
    servicio.save

    costo=Servicio.where(:id=>params[:servicio]).last
    if costo.description.index("puerta") or costo.description.index("caseta")
      uri = URI('https://lineaccess.mx/anet_devices/sendToDevice')
      https = Net::HTTP.new(uri.host,uri.port)
      https.use_ssl = true
      https.verify_mode = OpenSSL::SSL::VERIFY_NONE
      req = Net::HTTP::Post.new(uri.path,initheader = {'Content-Type' =>'application/x-www-form-urlencoded'})
      req['device'] = 'ce813f6e'
      req['content'] = 'ON'
      req.body="device=ce813f6e&content=ON"

      res = https.request(req)
    end


    recipientCard=Entrance.where(:id=>costo.owner_id).last.confirmation
    currentDude=Entrance.where(:id=>session[:cuser]).last
    extractMoneyFrom=currentDude.confirmation
    transaccion=sprintf('%019d', rand(17**17))
    transaccion=transaccion[0..18]
    # transaccion="9144510001324131299"
    puts transaccion

    uri = URI.parse "http://dmartin.org:8001/moneysend/v2/transfer?Format=XML"
    request = Net::HTTP::Post.new uri.path
    t = Time.now                        
    request.body = '<?xml version="1.0" encoding="UTF-8"?>
      <TransferRequest>
         <LocalDate>'+t.strftime("%m%d")+'</LocalDate>
         <LocalTime>'+t.strftime("%H%M%S")+'</LocalTime>
         <TransactionReference>'+transaccion+'</TransactionReference>

         <SenderName>'+currentDude.nombre+'</SenderName>
         <SenderAddress>
            <Line1>Mexico City</Line1>

            <City>Mexico City</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>78437</PostalCode>
            <Country>MEX</Country>
         </SenderAddress>
         <FundingCard>
            <AccountNumber>'+extractMoneyFrom+'</AccountNumber>
            <ExpiryMonth>11</ExpiryMonth>
            <ExpiryYear>2016</ExpiryYear>
         </FundingCard>

         <FundingMasterCardAssignedId>123456</FundingMasterCardAssignedId>
         <FundingAmount>
            <Value>'+costo.price+'</Value>
            <Currency>484</Currency>
         </FundingAmount>
         <ReceiverName>'+Entrance.where(:id=>costo.owner_id).last.nombre+'</ReceiverName>
         <ReceiverAddress>
            <Line1>Coso</Line1>
            <Line2>PO BOX 12</Line2>
            <City>Mexico DF</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>79906</PostalCode>
            <Country>MEX</Country>
         </ReceiverAddress>
         <ReceiverPhone>8180829367</ReceiverPhone>
         <ReceivingCard>
            <AccountNumber>'+recipientCard+'</AccountNumber>
         </ReceivingCard>
         <ReceivingAmount>
            <Value>182206</Value>
            <Currency>484</Currency>
         </ReceivingAmount>
         <Channel>W</Channel>
         <UCAFSupport>true</UCAFSupport>
         <ICA>009674</ICA>
         <ProcessorId>9000000442</ProcessorId>
         <RoutingAndTransitNumber>990442082</RoutingAndTransitNumber>
         <CardAcceptor>
            <Name>My Local Bank</Name>
            <City>Saint Louis</City>
            <State>MO</State>
            <PostalCode>63101</PostalCode>
            <Country>USA</Country>
         </CardAcceptor>
        <TransactionDesc>P2P</TransactionDesc>
        <MerchantId>123456</MerchantId>
      </TransferRequest>'
      request.content_type = 'application/xml'
      response = Net::HTTP.new(uri.host, uri.port).start { |http| http.request request }
      puts response.body.inspect



    respond_to do |format|        
        format.json { render json: servicio }
    
    end

  end


  def yomero
    # necesita parametros id de usuario y id de servicio
    # params[:servicio]=ID_servicio
    if params.has_key?(:myid)
      session[:cuser]=params[:myid]
    end
    servicio=Mastercard.new
    servicio.id_user=session[:cuser]
    servicio.servicio_id=params[:servicio]
    servicio.save

    costo=Servicio.where(:id=>params[:servicio]).last


    systemCard="5184680430000030"
    currentDude=Entrance.where(:id=>session[:cuser]).last
    extractMoneyFrom=currentDude.confirmation
    transaccion=sprintf('%019d', rand(17**17))
    transaccion=transaccion[0..18]
    # transaccion="9144510001324131299"
    puts transaccion

    uri = URI.parse "http://dmartin.org:8001/moneysend/v2/transfer?Format=XML"
    request = Net::HTTP::Post.new uri.path
    t = Time.now                        
    request.body = '<?xml version="1.0" encoding="UTF-8"?>
      <TransferRequest>
         <LocalDate>'+t.strftime("%m%d")+'</LocalDate>
         <LocalTime>'+t.strftime("%H%M%S")+'</LocalTime>
         <TransactionReference>'+transaccion+'</TransactionReference>

         <SenderName>'+currentDude.nombre+'</SenderName>
         <SenderAddress>
            <Line1>Mexico City</Line1>

            <City>Mexico City</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>78437</PostalCode>
            <Country>MEX</Country>
         </SenderAddress>
         <FundingCard>
            <AccountNumber>'+extractMoneyFrom+'</AccountNumber>
            <ExpiryMonth>11</ExpiryMonth>
            <ExpiryYear>2016</ExpiryYear>
         </FundingCard>

         <FundingMasterCardAssignedId>123456</FundingMasterCardAssignedId>
         <FundingAmount>
            <Value>'+costo.price+'</Value>
            <Currency>484</Currency>
         </FundingAmount>
         <ReceiverName>Coso system</ReceiverName>
         <ReceiverAddress>
            <Line1>Coso</Line1>
            <Line2>PO BOX 12</Line2>
            <City>Mexico DF</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>79906</PostalCode>
            <Country>MEX</Country>
         </ReceiverAddress>
         <ReceiverPhone>8180829367</ReceiverPhone>
         <ReceivingCard>
            <AccountNumber>'+systemCard+'</AccountNumber>
         </ReceivingCard>
         <ReceivingAmount>
            <Value>182206</Value>
            <Currency>484</Currency>
         </ReceivingAmount>
         <Channel>W</Channel>
         <UCAFSupport>true</UCAFSupport>
         <ICA>009674</ICA>
         <ProcessorId>9000000442</ProcessorId>
         <RoutingAndTransitNumber>990442082</RoutingAndTransitNumber>
         <CardAcceptor>
            <Name>My Local Bank</Name>
            <City>Saint Louis</City>
            <State>MO</State>
            <PostalCode>63101</PostalCode>
            <Country>USA</Country>
         </CardAcceptor>
        <TransactionDesc>P2P</TransactionDesc>
        <MerchantId>123456</MerchantId>
      </TransferRequest>'
      request.content_type = 'application/xml'
      response = Net::HTTP.new(uri.host, uri.port).start { |http| http.request request }
      puts response.body.inspect



    respond_to do |format|        
        format.json { render json: servicio }
    
    end

  end

  # GET /mastercards/new
  def new
    @mastercard = Mastercard.new
  end

  # GET /mastercards/1/edit
  def edit
  end

  # POST /mastercards
  # POST /mastercards.json
  def create
    @mastercard = Mastercard.new(mastercard_params)

    respond_to do |format|
      if @mastercard.save
        format.html { redirect_to @mastercard, notice: 'Mastercard was successfully created.' }
        format.json { render :show, status: :created, location: @mastercard }
      else
        format.html { render :new }
        format.json { render json: @mastercard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mastercards/1
  # PATCH/PUT /mastercards/1.json
  def update
    respond_to do |format|
      if @mastercard.update(mastercard_params)
        format.html { redirect_to @mastercard, notice: 'Mastercard was successfully updated.' }
        format.json { render :show, status: :ok, location: @mastercard }
      else
        format.html { render :edit }
        format.json { render json: @mastercard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mastercards/1
  # DELETE /mastercards/1.json
  def destroy
    @mastercard.destroy
    respond_to do |format|
      format.html { redirect_to mastercards_url, notice: 'Mastercard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mastercard
      @mastercard = Mastercard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mastercard_params
      params.require(:mastercard).permit(:id,:id_user, :servicio_id, :status)
    end
end
