class GeofencingsController < ApplicationController
  before_action :set_geofencing, only: [:show, :edit, :update, :destroy]

  # GET /geofencings
  # GET /geofencings.json
  def index
    @geofencings = Geofencing.all
  end

  # GET /geofencings/1
  # GET /geofencings/1.json
  def show
  end

  # GET /geofencings/new
  def new
    @geofencing = Geofencing.new
  end

  # GET /geofencings/1/edit
  def edit
  end

  # POST /geofencings
  # POST /geofencings.json
  def create
    @geofencing = Geofencing.new(geofencing_params)

    respond_to do |format|
      if @geofencing.save
        format.html { redirect_to @geofencing, notice: 'Geofencing was successfully created.' }
        format.json { render :show, status: :created, location: @geofencing }
      else
        format.html { render :new }
        format.json { render json: @geofencing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geofencings/1
  # PATCH/PUT /geofencings/1.json
  def update
    respond_to do |format|
      if @geofencing.update(geofencing_params)
        format.html { redirect_to @geofencing, notice: 'Geofencing was successfully updated.' }
        format.json { render :show, status: :ok, location: @geofencing }
      else
        format.html { render :edit }
        format.json { render json: @geofencing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geofencings/1
  # DELETE /geofencings/1.json
  def destroy
    @geofencing.destroy
    respond_to do |format|
      format.html { redirect_to geofencings_url, notice: 'Geofencing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geofencing
      @geofencing = Geofencing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geofencing_params
      params.require(:geofencing).permit(:nombre, :entrance_id, :area)
    end
end
