#encoding: utf-8
include ActionView::Helpers::TextHelper
require 'securerandom'
require 'email_validator'
require 'date'
class EntrancesController < ApplicationController
  before_action :set_entrance, only: [:show, :edit, :update, :destroy,:setUserDetails]
  protect_from_forgery except: [:createToken]
  def createToken
    respuesta=Hash.new
    respuesta[:token]=form_authenticity_token.to_s
    respuesta[:me]=session[:cuser]
    respond_to do |format|
        format.json { render json: respuesta.to_json  }
      end
  end

  def getAllUsersAsJson
    respuesta=Entrance.all
    respond_to do |format|
        format.json { render json: respuesta.to_json  }
      end
  end

  def getUserDetails
    if session[:cuser]==nil
      respond_to do |format|
        format.html { redirect_to "/login" }
      end
      return
    end

    respond_to do |format|
        format.json { render json: Entrance.find(session[:cuser]).to_json  }
    end

    
  end


  def setUserDetails
    if session[:cuser]==nil
      respond_to do |format|
        format.html { redirect_to "/login" }
      end
      return
    end
    # nombre:jQuery("#uname").val(),
    # apellido:jQuery("#apellido").val(),
    # shortdescription:jQuery("#shortdescription").val(),
    # phone:jQuery("#phone").val(),
    # email:jQuery("#email").val(),
    # schedule:jQuery("#schedule").val(),
    # address:jQuery("#address").val(),
    # about:jQuery("#aboutyou").val()
    @entrance = Entrance.new(entrance_params)
    if @entrance.id==session[:cuser]
      # @entrance=Entrance.find(session[:cuser])
      @entrance.nombre=params[:nombre]
      @entrance.apellido=params[:apellido]
      # usuario.shortdescription=params[:shortdescription]
      @entrance.phone=params[:phone]
      @entrance.email=params[:email]
      @entrance.save
      # usuario.horario=params[:schedule]
      # usuario.fulldescription=params[:about]
      # usuario.address=params[:address]
    end
    

    
    respond_to do |format|
        format.json { render json: Entrance.find(session[:cuser]).to_json  }
    end

    
  end


  def logout
    session[:cuser]=nil
    respond_to do |format|
        format.html { redirect_to "/login" }
    end
  end
  def createAccount
    verificar=Entrance.where(:email=>params[:uremail]).last
    verificar2=Entrance.where(:phone=>params[:phone]).last
    mensajes=Hash.new
    mensajes[:title]="Info"
    mensajes[:content]="La cuenta se ha creado con éxito"
    cuentaNueva=nil
    begin
      if verificar2!=nil
        mensajes[:title]="Error"
        mensajes[:content]="El telefono ya esta registrado"
        raise 'An error has occured'
      end
      if verificar!=nil
        mensajes[:title]="Error"
        mensajes[:content]="Ya hay una cuenta con esos datos"
        raise 'An error has occured'
      end
      if params[:urpassword]!=params[:urrepeat]
        mensajes[:title]="Error"
        mensajes[:content]="Las contraseñas no coinciden"
        raise 'An error has occured'
      end
      if EmailValidator.valid?(params[:uremail])==false
        mensajes[:title]="Error"
        mensajes[:content]="El e-mail que diste no parece ser válido"
        raise 'An error has occured'
      end


      cuentaNueva=Entrance.new
      cuentaNueva.email=params[:uremail]  
      cuentaNueva.phone=params[:phone]
      cuentaNueva.color=params[:color]
      cuentaNueva.marca=params[:marca]
      cuentaNueva.modelo=params[:modelo]
      cuentaNueva.placa=params[:placa]
      cuentaNueva.nombre=params[:urname]
      cuentaNueva.hexicode=params[:urpassword]
      cuentaNueva.confirmation=params[:creditcard]

      if params.has_key?(:ursex)
        cuentaNueva.sexo=params[:ursex]
      else
        cuentaNueva.sexo="M"
      end
       if params.has_key?(:urlastn)
        cuentaNueva.apellido=params[:urlastn]
      end
      if params.has_key?(:birthday)
        fechaBirthdaySplit=params[:birthday].split("/")
        if fechaBirthdaySplit.length!=3
          mensajes[:title]="Error"
          mensajes[:content]="Tu cumpleaños parece una fecha no valida"
        raise 'An error has occured'
        end
        fechaBirthday=DateTime.new(fechaBirthdaySplit[2].to_i,fechaBirthdaySplit[1].to_i,fechaBirthdaySplit[0].to_i)
        cuentaNueva.birthday=fechaBirthday
      else
        cuentaNueva.birthday=DateTime.now
      end
      cuentaNueva.save
      # creamos una cuenta virtual
      plastico=Plastic.new
      plastico.tokenized=params[:tokenized]
      plastico.user_id=cuentaNueva.id
      plastico.save
      sendMandrill([params[:uremail]],t(:welcomeEmailTitle),t(:welcomeEmailContent),"welcome_mail")
      session[:cuser]=cuentaNueva.id

      respond_to do |format|
        
        if params.has_key?(:apptoken)
          mensajes[:title]="¡Perfecto!"
          mensajes[:content]="Tu cuenta se ha creado, ya puedes hacer uso de ella."
          format.json { render json: mensajes.to_json }  
        else
          format.html { redirect_to "/welcomes" }
        end
        
      end
      
    rescue Exception => e
      puts e.inspect
      respond_to do |format|
        
        if params.has_key?(:apptoken)
          format.json { render json: mensajes.to_json }
        else
          format.html { redirect_to "/register", notice: t(:errorregistermail) }
        end
      end
    end    
    
  end

  def resetAccount
    # reseteamos cuenta y enviamos mail
    supuestoReset=Entrance.where(:email=>params[:emailret]).last
    if supuestoReset!=nil
      # enviamos un mail con la nueva contraseña
      supuestoReset.hexicode=SecureRandom.hex.to_s[0..5]
      supuestoReset.save
      sendMandrill([params[:emailret]],"PIN reset","Hemos detectado que deseas resetear tu PIN, tu nuevo PIN de ahora en adelante será:"+supuestoReset.hexicode,"reset_password")
    end
    tmsg=t(:forgetmessage)
    respond_to do |format|
        format.html { redirect_to "/forget", notice: t(:forgetmessage) }
        format.json { render json: tmsg.to_json }
    end
  end

  def to_hash(objeto)
    hash = {}; objeto.attributes.each { |k,v| hash[k] = v }
    return hash
  end

  def getParkingLot
    # obtener todas las campañas marcadas como parkinglot y obtener sus posiciones
    parkings=Campaign.where(:parkinglot=>1).where('stoprewardsdate>=?',DateTime.now)
    # ahora verifcar que exista un pago valido por cada lugar
    finalPromos=[]
    parkings.each do |singleParking|
      # contar las impresiones
      impresiones=Beaconlog.where(:campaing_id=>singleParking.id).length
      if impresiones<singleParking.impresiones_meta
        # aun tiene impresiones disponibles
        
        # buscamos el pago de la campaña para ver si aun tiene credito
        pago=Pago.find_by_campaign_id(singleParking.id)
        if pago.credito.to_f>0
          # buscar el beacon al que le pertenece
          beacon=Beacon.find_by_id(pago.beacons_id)
          lugar=Hash.new
          lugar=to_hash(singleParking)
          lugar["lat"]=beacon.latitud
          lugar["lng"]=beacon.longitud
          finalPromos.push(lugar)
        end
      end
    end
    respond_to do |format|
      format.json { render json: finalPromos.to_json }
    end
  end

  def confirmAccount
    respond_to do |format|
        format.html { redirect_to "/login", notice: 'Entrance was successfully created.' }
        format.json { render :show, status: :created, location: "/login" }
    end
  end
  def getUserPromos
    # aqui verificamos todas las promociones activas del usuario que no sean mensajes
    promos=Beaconlog.where(:client_id=>session[:cuser],:activo=>nil)
    #>>>> 22 junio 2015 se agrega leer mensajes del sistema
    # se cargan los mensajes del sistema que esten activos aun
    systemMsg=Campaign.where(:systemmsg=>1).where('stoprewardsdate>=?',DateTime.now)
    # se agregan a las promociones como si existieran virtualmente
    systemMsg.each do |singleSystemMsg|
      newFakePromo=Beaconlog.new
      newFakePromo.client_id=session[:cuser]
      newFakePromo.campaing_id=singleSystemMsg.id
      promos.push(newFakePromo)
    end
    # <<<<<


    finalPromos=[]
    # ahora hay que descartar las que no son promociones sino mensajes
    promos.each do |singlePromo|
      campaign=Campaign.where(:id=>singlePromo.campaing_id).where('stoprewardsdate>=?',DateTime.now).where("",).last
      if campaign!=nil
         if (campaign.cupon==0 or campaign.cupon==2 or campaign.cupon==3) and campaign.visibleonsite!=1
          tcampana=Hash.new
          tcampana[:url]=campaign.imagen
          tcampana[:descripcion]=campaign.descripcion
          tcampana[:negocio]=campaign.nombre
          tcampana[:timer]=campaign.tiempo_permanencia
          tcampana[:popup]=campaign.cupon
          tcampana[:id]=campaign.id
          tcampana[:price]=campaign.price
          tcampana[:reward]=singlePromo.id
          tcampana[:promo]=campaign.promocode
          tcampana[:fechafin]=campaign.stoprewardsdate
          tcampana[:likes]=Like.where(:campaign_id=>campaign.id).length
          tcampana[:fechatext]=campaign.stoprewardsdate.strftime("%b,%a,%d").downcase
          # >>>>>> determinar si este cupon es un producto
          tcampana[:is_product]=singlePromo.is_product
          # <<<<<<
          if campaign.stoprewardsdate!=nil
            if campaign.stoprewardsdate>=DateTime.now
              enTexto=time_diff_in_natural_language(DateTime.now,campaign.stoprewardsdate).to_s
              if enTexto.index("year")!=nil
                enTexto["year"]="año"  
              end
              if enTexto.index("month")!=nil
              enTexto["month"]="mes"
              end
              if enTexto.index("day")!=nil
              enTexto["day"]="día"
              end
              if enTexto.index("hour")!=nil
              enTexto["hour"]="hora"
              end
              if enTexto.index("minute")!=nil
              enTexto["minute"]="minuto"
              end
               if enTexto.index("week")!=nil
              enTexto["week"]="semana"
              end
              tcampana[:remaining]=enTexto
            else
              tcampana[:remaining]="Promoción vencida"
            end

          else
            tcampana[:remaining]="Sin fecha de caducidad"
          end
          if singlePromo.end_date==nil or singlePromo.end_date>Time.now
            if (singlePromo.is_product==1 and Pago.where(:beaconlog_id=>singlePromo.id).length>0) or (singlePromo.is_product==nil or singlePromo.is_product==0)
              finalPromos.push(tcampana)    
            end
          end
            
          
        end
      end
    end

    if params[:apptoken]=="BlimbioAdmin"
      finalPromos=Publicidad.where(:entrances_id=>session[:cuser])
      # buscamos la publicidad
    end

    respond_to do |format|
      format.json { render json: finalPromos.to_json }
    end
  end

  def time_diff_in_natural_language(from_time, to_time)
    from_time = from_time.to_time if from_time.respond_to?(:to_time)
    to_time = to_time.to_time if to_time.respond_to?(:to_time)
    distance_in_seconds = ((to_time - from_time).abs).round
    components = []

    %w(year month week day hour minute).each do |interval|
      # For each interval type, if the amount of time remaining is greater than
      # one unit, calculate how many units fit into the remaining time.
      if distance_in_seconds >= 1.send(interval)
        delta = (distance_in_seconds / 1.send(interval)).floor
        distance_in_seconds -= delta.send(interval)
        components << pluralize(delta, interval)
      end
    end

    enTexto=components.join(", ")
    if enTexto.index("year")!=nil
      enTexto["year"]="año"  
    end
    if enTexto.index("month")!=nil
    enTexto["month"]="mes"
    end
    if enTexto.index("day")!=nil
    enTexto["day"]="día"
    end
    if enTexto.index("hour")!=nil
    enTexto["hour"]="hora"
    end
    if enTexto.index("minute")!=nil
    enTexto["minute"]="minuto"
    end
     if enTexto.index("week")!=nil
    enTexto["week"]="semana"
    end
    return enTexto
  end

  def loginAccount
    if Entrance.all.length==0
      master=Entrance.new
      master.email="andres.santos@consultware.mx"
      master.hexicode="123"
      master.nombre="Master"
      master.apellido="Root"
      master.birthday=Time.now-20.years
      master.sexo="M"
      master.superlevel=1000
      master.save
    end
    usuario=params[:username]
    mypwd=params[:password]
    mensajes=Hash.new
    mensajes[:title]="Error"
    mensajes[:content]="Error"
    usuario=Entrance.where(:email=>usuario,:hexicode=>mypwd).last
    
    if usuario==nil
      mensajes[:content]="No se encontraron datos con tu usuario. ¿Ya intentaste registrarte?"
      respond_to do |format|
        if params.has_key?(:apptoken)
          format.json { render json: mensajes.to_json }
        else 
          format.html { redirect_to "/login", notice: 'error' }
        end
      end  
    else 
    session[:cuser]=usuario.id
    session[:cname]=usuario.nombre
    session[:cemail]=usuario.email
      respond_to do |format|
        if params.has_key?(:apptoken)
          mensajes[:title]="OK_WELCOME"
          mensajes[:content]="OK_WELCOME"
          format.json { render json: mensajes.to_json }
        else
          if usuario.isclient==1 or usuario.superlevel>=1000
            format.html { redirect_to "/welcomes" }
          else
            session[:cuser]=nil
            session[:cname]=nil
            session[:cemail]=nil
            format.html { redirect_to "/login",notice: 'error' }
            
          end
        end
      end
    end
  end
  # GET /entrances
  # GET /entrances.json
  def index
    
    @entrances = Entrance.all
  end

  # GET /entrances/1
  # GET /entrances/1.json
  def show
  end

  # GET /entrances/new
  def new
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
    @entrance = Entrance.new
  end

  def history
    finalPromos=[]
    preExtraction=[]
    # traer todos los cupones vencidos y si es dueño, las reservas
    # cargar primero las reservaciones en caso de que existan, extrayendo
    # las campañas
    parkings=Campaign.where(:entrances_id=>session[:cuser],:parkinglot=>1)
    # ahora extraer del log las reservas hechas de esa campaña
    # cargar el resto de las promociones como si fuera cliente normal
    afterpromos=Beaconlog.where(:campaing_id =>parkings.index_by(&:id).keys)
    afterpromos.each do |singleafter|
      preExtraction.push(singleafter)
    end
    promos=Beaconlog.where(:client_id=>session[:cuser]).where("activo=0 or end_date<?",Time.now)
    promos.each do |singleex|
      preExtraction.push(singleex)
    end
    
    preExtraction.each do |singlePromo|
      campaign=Campaign.where(:id=>singlePromo.campaing_id).last
      tcampana=Hash.new
      tcampana[:url]=campaign.imagen
      tcampana[:descripcion]=campaign.descripcion
      tcampana[:negocio]=campaign.nombre
      tcampana[:timer]=campaign.tiempo_permanencia
      tcampana[:popup]=campaign.cupon
      tcampana[:id]=campaign.id
      tcampana[:price]=campaign.price
      tcampana[:reward]=singlePromo.id
      tcampana[:promo]=campaign.promocode
      tcampana[:fechafin]=campaign.stoprewardsdate
      tcampana[:likes]=Like.where(:campaign_id=>campaign.id).length
      tcampana[:fechatext]=campaign.stoprewardsdate.strftime("%b,%a,%d").downcase
      # >>>>>> determinar si este cupon es un producto
      tcampana[:is_product]=singlePromo.is_product
      # <<<<<<

      # >>>>> si es un estacionamiento, regresar las placas y color
      if campaign.parkinglot==1
        # placas del usuario
        tcampana[:placas]="Placa: "+Entrance.find_by_id(singlePromo.client_id).placa
      end
      # <<<<<
      if campaign.stoprewardsdate!=nil
        if campaign.stoprewardsdate>=DateTime.now
          enTexto=time_diff_in_natural_language(DateTime.now,campaign.stoprewardsdate).to_s
          
          tcampana[:remaining]=enTexto
        else
          tcampana[:remaining]="Promoción vencida"
        end

      else
        tcampana[:remaining]="Sin fecha de caducidad"
      end
      if singlePromo.end_date!=nil
        if singlePromo.end_date>Time.now
          tcampana[:remaining]="Vence "+time_diff_in_natural_language(DateTime.now,singlePromo.end_date).to_s  

        else
          tcampana[:remaining]="Recompensa vencida"
        end
      end
      if singlePromo.rewardeddate!=nil
        tcampana[:remaining]="Recompensa canjeada"
      end

      
      finalPromos.push(tcampana)    
    end
    respond_to do |format|
      format.json { render json: finalPromos.to_json }
    end
  end

  # GET /entrances/1/edit
  def edit
    if session[:cuser]==nil
    respond_to do |format|
        format.html { redirect_to "/login" }
    end
  end
  end

  # POST /entrances
  # POST /entrances.json
  def create
    @entrance = Entrance.new(entrance_params)
    if @entrance.sexo==nil
      @entrance.sexo="H"
    end
    respond_to do |format|
      if @entrance.save
        format.html { redirect_to @entrance, notice: 'Entrance was successfully created.' }
        format.json { render :show, status: :created, location: @entrance }
      else
        format.html { render :new }
        format.json { render json: @entrance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entrances/1
  # PATCH/PUT /entrances/1.json
  def update
    respond_to do |format|
      if @entrance.update(entrance_params)
        plastico=Plastic.new
        plastico.tokenized=params[:tokenized]
        plastico.user_id=session[:cuser]
        plastico.save
        format.html { redirect_to @entrance, notice: 'Entrance was successfully updated.' }
        format.json { render :show, status: :ok, location: @entrance }
      else
        format.html { render :edit }
        format.json { render json: @entrance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entrances/1
  # DELETE /entrances/1.json
  def destroy
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser==nil
      redirect_to "/login"
      return
    end
    if currentUser.superlevel<1000
      redirect_to "/login"
      return
    end
    @entrance.destroy
    respond_to do |format|
      format.html { redirect_to entrances_url, notice: 'Entrance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entrance
      if params[:id]=="login" or params[:id]=="register" or params[:id]=="forget"
        @entrance=Entrance.new
      else
        if Entrance.find(session[:cuser]).superlevel>=1000
          @entrance = Entrance.find(params[:id])  
        else
          @entrance = Entrance.find(session[:cuser])  
        end
      end
      
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entrance_params
      params.require(:entrance).permit(:nombre, :apellido, :email, :empresa, :rfc,:birthday,:sexo,:superlevel,:isclient,:shortdescription,:fulldescription,:phone,:horario,:address,:id, :hexicode, :color, :marca, :modelo, :placa)
    end
end
