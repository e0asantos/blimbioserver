# encoding: UTF-8
class IopushesController < ApplicationController
  before_action :set_iopush, only: [:show, :edit, :update, :destroy]

  # GET /iopushes
  # GET /iopushes.json
  def index
    @iopushes = Iopush.all
  end

  def updateToken
    if session[:cuser]==nil
      respond_to do |format|
        format.html { redirect_to "/login" }
      end
      return
    end

    verifyToken=Iopush.where(:user_id=>session[:cuser],:token=>params[:token],:device_id=>params[:device_id])
    if verifyToken.length==0
      newToken=Iopush.new
      newToken.user_id=session[:cuser]
      newToken.token=params[:token]
      newToken.save
    end

    respond_to do |format|
      format.json { render json: "OK".to_json }
    end
  end

  def generateHistory
    

    # Create a PushService instance for sending notifications
    # The constructor can take a hash of parameters including:
    #   device_tokens - An array of device tokens to push to
    #   message - a hash the represents the message to send  
    usuario=Entrance.where(:email=>"andres.santos@consultware.mx").last
    mobileNotification(Iopush.where(:user_id=>usuario.id).index_by(&:token).keys,"nada")
    respond_to do |format|
      format.json { render json: "OK".to_json }
    end
  end

  # GET /iopushes/1
  # GET /iopushes/1.json
  def show
  end

  # GET /iopushes/new
  def new
    @iopush = Iopush.new
  end

  # GET /iopushes/1/edit
  def edit
  end

  # POST /iopushes
  # POST /iopushes.json
  def create
    @iopush = Iopush.new(iopush_params)

    respond_to do |format|
      if @iopush.save
        format.html { redirect_to @iopush, notice: 'Iopush was successfully created.' }
        format.json { render :show, status: :created, location: @iopush }
      else
        format.html { render :new }
        format.json { render json: @iopush.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /iopushes/1
  # PATCH/PUT /iopushes/1.json
  def update
    respond_to do |format|
      if @iopush.update(iopush_params)
        format.html { redirect_to @iopush, notice: 'Iopush was successfully updated.' }
        format.json { render :show, status: :ok, location: @iopush }
      else
        format.html { render :edit }
        format.json { render json: @iopush.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /iopushes/1
  # DELETE /iopushes/1.json
  def destroy
    @iopush.destroy
    respond_to do |format|
      format.html { redirect_to iopushes_url, notice: 'Iopush was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_iopush
      @iopush = Iopush.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def iopush_params
      params.require(:iopush).permit(:device_id, :user_id, :token)
    end
end
