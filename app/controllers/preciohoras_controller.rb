class PreciohorasController < ApplicationController
  before_action :set_preciohora, only: [:show, :edit, :update, :destroy]

  # GET /preciohoras
  # GET /preciohoras.json
  def index
    @preciohoras = Preciohora.all
  end

  # GET /preciohoras/1
  # GET /preciohoras/1.json
  def show
  end

  # GET /preciohoras/new
  def new
    @preciohora = Preciohora.new
  end

  # GET /preciohoras/1/edit
  def edit
  end

  # POST /preciohoras
  # POST /preciohoras.json
  def create
    @preciohora = Preciohora.new(preciohora_params)

    respond_to do |format|
      if @preciohora.save
        format.html { redirect_to "/precios", notice: 'Preciohora was successfully created.' }
        format.json { render :show, status: :created, location: @preciohora }
      else
        format.html { render :new }
        format.json { render json: @preciohora.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /preciohoras/1
  # PATCH/PUT /preciohoras/1.json
  def update
    respond_to do |format|
      if @preciohora.update(preciohora_params)
        format.html { redirect_to @preciohora, notice: 'Preciohora was successfully updated.' }
        format.json { render :show, status: :ok, location: @preciohora }
      else
        format.html { render :edit }
        format.json { render json: @preciohora.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /preciohoras/1
  # DELETE /preciohoras/1.json
  def destroy
    @preciohora.destroy
    respond_to do |format|
      format.html { redirect_to preciohoras_url, notice: 'Preciohora was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_preciohora
      @preciohora = Preciohora.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def preciohora_params
      params.require(:preciohora).permit(:hora, :plan, :precio,:horafin)
    end
end
