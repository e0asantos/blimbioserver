require 'net/http'

class ServiciosController < ApplicationController
  before_action :set_servicio, only: [:show, :edit, :update, :destroy]

  # GET /servicios
  # GET /servicios.json
  def index
    @servicios = Servicio.all
  end

  # GET /servicios/1
  # GET /servicios/1.json
  def show
  end

  def makeProductPayment
    # who make the payment
  end

  def showServicesByLocation
    if params.has_key?(:myid)
      session[:cuser]=params[:myid]
    end
    if params.has_key?(:uuid)
      serviciosCercanos=Servicio.where(:uid=>params[:uuid])
      respond_to do |format|
        format.json { render json: serviciosCercanos.to_json }
      end
    end
    if params.has_key?(:busqueda)
      pBusqueda=nil
      primariaSearch=Mastercard.where(:id_user=>session[:cuser])
      excluir=Mastercard.where("id_user!="+session[:cuser].to_s)
      excluirID=excluir.map(&:servicio_id)
      onlyIDs=primariaSearch.map(&:servicio_id)
      onlyIDs.delete(nil)
      excluirID.delete(nil)
      items_table = Arel::Table.new(:servicios)

      array_without_excluded_ids = Servicio.where(items_table[:id].not_in onlyIDs)
      puts "......."
      puts session[:cuser]
      puts onlyIDs.inspect
      puts array_without_excluded_ids.inspect
      puts "......."

      if params[:busqueda]=="*"
        pBusqueda=Servicio.where(items_table[:id].not_in excluirID)
      else
        pBusqueda=Servicio.where("description like '%"+params[:busqueda]+"%'").where(items_table[:id].not_in excluirID)
      end
      pBusqueda.each do |singleReg|
        seleccionarEnvio=Mastercard.where(:id_user=>session[:cuser],:servicio_id=>singleReg.id).last
        if seleccionarEnvio!=nil
          singleReg.asignado=1
        end
      end
      respond_to do |format|
        format.json { render json: pBusqueda.to_json }
      end
    end
    
  end

  # GET /servicios/new
  def new
    @servicio = Servicio.new
  end

  # GET /servicios/1/edit
  def edit
  end

  # POST /servicios
  # POST /servicios.json
  def create
    @servicio = Servicio.new(servicio_params)
    @servicio.owner_id=session[:cuser]
    # lets generate a callback to mastercard service with both the cards
    
    respond_to do |format|
      if @servicio.save
        format.html { redirect_to @servicio, notice: 'Servicio was successfully created.' }
        format.json { render :show, status: :created, location: @servicio }
      else
        format.html { render :new }
        format.json { render json: @servicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /servicios/1
  # PATCH/PUT /servicios/1.json
  def update
    respond_to do |format|
      if @servicio.update(servicio_params)
        format.html { redirect_to @servicio, notice: 'Servicio was successfully updated.' }
        format.json { render :show, status: :ok, location: @servicio }
      else
        format.html { render :edit }
        format.json { render json: @servicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servicios/1
  # DELETE /servicios/1.json
  def destroy
    @servicio.destroy
    respond_to do |format|
      format.html { redirect_to servicios_url, notice: 'Servicio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_servicio
      @servicio = Servicio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def servicio_params
      params.require(:servicio).permit(:uid, :description, :price, :owner_id, :tip, :category,:asignado)
    end
end
