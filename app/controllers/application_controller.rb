# encoding: UTF-8
require 'mandrill'
require 'openpay'
require 'uri'
require 'net/http'
require 'net/https'
require 'one_signal'
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def forceDevelopment
    return 1
  end
  def mobileNotification tokenArray, message
    if tokenArray.length==0
      return
    end
    # puts "---pushin"
    # OneSignal::OneSignal.api_key = "MmI1YTMyNDItNDU5MC00ZDk0LTg5N2QtZDZkZTY1ZjczMzgz"
    # OneSignal::OneSignal.user_auth_key = "NzE2ZDM0ZmEtMDVjYy00ZjYwLWFiMmMtYzZhYmYxOTllNDUy"

    # # create app
    # paramsapp = {
    #   name: 'Coinless'
    # }
    # response = OneSignal::App.create(params: paramsapp)
    # app_id = JSON.parse(response.body)["id"]
    # puts "--- created app id: #{app_id}"

    # finalTokens=[]
    # tokenArray.each do |singleToken|
    #   device_token = singleToken
    #   paramstoken = {
    #     app_id: app_id,
    #     device_type: 0,
    #     identifier: device_token
    #   }
    #   response = OneSignal::Player.create(params: paramstoken)
    #   player_id = JSON.parse(response.body)["id"]
    #   puts "--- created player id: #{player_id}"
    # end


    # finalPush = {
    #   app_id: app_id,
    #   contents: {
    #     en: 'hello player'
    #   },
    #   ios_badgeType: 'None',
    #   ios_badgeCount: 1,
    #   include_player_ids: finalTokens
    # }

    # begin
    #   response = OneSignal::Notification.create(params: finalPush)
    #   notification_id = JSON.parse(response.body)["id"]
    # rescue OneSignal::OneSignalError => e
    #   puts "--- OneSignalError  :"
    #   puts "-- message : #{e.message}"
    #   puts "-- status : #{e.http_status}"
    #   puts "-- body : #{e.http_body}"
    # end

    # intercambiar tokens por user id
    userids=Iopush.where(:token=>tokenArray).index_by(&:device_id).keys
    @bodyToSend=Hash.new
    @bodyToSend={"app_id"=>"54f1f14a-830f-48d3-bcdc-5518df677b81","contents"=>{"en"=>message,"es"=>message},"include_player_ids"=>userids}
    uri = URI.parse("https://onesignal.com/api/v1/notifications")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json','Authorization'=>'Basic MmI1YTMyNDItNDU5MC00ZDk0LTg5N2QtZDZkZTY1ZjczMzgz'})
    req['foo'] = 'bar'
    req.body = "#{@bodyToSend.to_json} "
    res = https.request(req)
    puts res.inspect


    # @bodyToSend=Hash.new
    # @bodyToSend["tokens"]=tokenArray
    # # if Rails.env.development?
    #   @bodyToSend["profile"]="development"  
    # # else
    # #   @bodyToSend["profile"]="production"
    # # end

    # @bodyToSend["notification"]={"message"=>message,"title"=>"Coinless","sound"=> "default","ios"=>{"sound"=> "default"}}
    # uri = URI.parse("https://api.ionic.io/push/notifications")
    # https = Net::HTTP.new(uri.host,uri.port)
    # https.use_ssl = true
    # req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json','Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwYzYwNDMyNS05ZjBiLTQ5MTgtODU3My01OTlkNDRmM2I5ZjkifQ.yEon0qOkm9-1R3f8RoyjFefb6UuK3LA3ElCQzvOybNo'})
    # req['foo'] = 'bar'
    # req.body = "#{@bodyToSend.to_json} "
    # res = https.request(req)
    # puts "Response #{res.code} #{res.message}: #{res.body}"



    
  end

  def openPayUser
    if Rails.env.development? or forceDevelopment()==1
      puts "===OPENPAY DEV"
      return "muen0ypxkc2xgsf6j8sq"
    else 
      puts "===OPENPAY PROD"
      return "m9oytzoeyay0ds5qnnm9"
    end
  end

  def openPaySecretKey
    if Rails.env.development?  or forceDevelopment()==1
      puts "===OPENPAY DEV"
      return "sk_29d9ae2c53764ac4903b19997d6100be"
    else 
      puts "===OPENPAY PROD"
      return "sk_6485a5a5381b4b1787d41d36e729285f"
    end
  end

  def sendMandrill(arrayEmail,titulo,contenido,evento)
  	mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
  	finalArrayEmail=[]
  	arrayEmail.each do |singleEmail|
  		cuenta=Entrance.where(:email=>singleEmail).last
  		nombre="Cliente Incuva"
  		if cuenta!=nil
  			nombre=cuenta.nombre+" "
  			if cuenta.apellido!=nil
  				nombre=nombre+cuenta.apellido
  			end
  		end
  		finalArrayEmail.push({"email"=>singleEmail,"name"=>"Cliente incuva","type"=>"to"})
  	end
  	# buscamos email template
  	emailTemplate=MailTemplate.where(:evento=>evento).last
  	htmlEmail=emailTemplate.template
  	htmlEmail[emailTemplate.mensaje]=contenido
  	htmlEmail[emailTemplate.titulo]=titulo
  	message = {"html"=>htmlEmail,"text"=>"Incuva idas soporte ",
       "subject"=>titulo,
       "from_email"=>"hola@blimb.io",
       "from_name"=>"Incuva ideas - Soporte",
       "to"=>finalArrayEmail,
       "headers"=>{"Reply-To"=>"message.reply@example.com"},
       "important"=>false,
       "track_opens"=>nil,
       "track_clicks"=>nil,
       "auto_text"=>nil,
       "auto_html"=>nil,
       "inline_css"=>nil,
       "url_strip_qs"=>nil,
       "preserve_recipients"=>nil,
       "view_content_link"=>nil,
       "tracking_domain"=>nil,
       "signing_domain"=>nil,
       "return_path_domain"=>nil,
       "merge"=>true,
       "global_merge_vars"=>[],
       "merge_vars"=>[],
       "tags"=>["password-resets"],
       "attachments"=>[],
       "images"=>[]}
  	async = false
	ip_pool = "Main Pool"
	send_at = DateTime.now-2.hours
	result = mandrill.messages.send message, async, ip_pool, send_at
  end
end
