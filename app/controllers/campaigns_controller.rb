#encoding: utf-8
require 'securerandom'
require 'pusher'
class CampaignsController < ApplicationController
  before_action :set_campaign, only: [:show, :edit, :update, :destroy]

  # GET /campaigdns
  # GET /campaigns.json
  def index
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    if Entrance.find_by_id(session[:cuser]).superlevel<1000
      redirect_to "/"
      return
    end
    @campaigns = Campaign.all
  end

  def productNotAvailable
    
  end

  def orderPlaced(userid,msg)
    # buscar los tokens del usuario
    tokens=Iopush.where(:user_id=>userid).index_by(&:token).keys
    mobileNotification(tokens,msg)
  end

  def notifyToOwner
    
  end

  def commitToBuy
    productid=params[:campaign]
    # traemos el producto
    thisProduct=Campaign.find_by_id(productid)
    # ensure price is not nil and is a number
    if thisProduct==nil
      respond_to do |format|
        format.json {render json: "NOT_PLACED".to_json}
      end  
      return
    end
    if thisProduct.price==nil
      respond_to do |format|
        format.json {render json: "NOT_PLACED".to_json}
      end  
      return
    end
    if thisProduct.price.to_f<10
      respond_to do |format|
        format.json {render json: "NOT_PLACED".to_json}
      end  
      return
    end
    
    
      # proceed to buy
      @openpay=OpenpayApi.new(openPayUser(),openPaySecretKey())
      @cards=@openpay.create(:cards)
    
      usuario=Entrance.find_by_id(session[:cuser])
      # puts usuario.inspect
      # nos ganamos un 10% adicional sobre el valor
      totalAmount=sprintf('%.2f',thisProduct.price.to_f+(thisProduct.price.to_f*0.1)).to_f
    begin
      @charges=@openpay.create(:charges)
      puts "Tiempo:"+DateTime.now.to_s
      customer_hash={
          "name" => usuario.nombre ,
          "last_name" => usuario.apellido,
          "phone_number" => usuario.phone,
          "email" => usuario.email
      }
      myorderid="coinless-"+SecureRandom.base64
      request_hash={
          "method" => "card",
          "source_id" => Plastic.where(:user_id=>usuario.id).last.tokenized,
          "amount" => totalAmount,
          "currency" => "MXN",
          "description" => "Pago inalambrico",
          "order_id" => myorderid,
          "customer" => customer_hash
      }
      puts request_hash.inspect
      # creamos una entrada de trueque
      verificacion=Pago.new
      verificacion.usuarios_id=session[:cuser]
      # verificacion.order_id=order_id
      # verificacion.notes="Paqueteria on-demand"
      verificacion.cantidad_original=thisProduct.price
      verificacion.cantidad_final=totalAmount
      verificacion.campaign_product=thisProduct.id
      verificacion.paymentuid=myorderid
      verificacion.fechainicio=Time.now
      # verificacion.ondemand=1
      verificacion.save
       cargo_de_prueba=@charges.create(request_hash)
       puts cargo_de_prueba.inspect
       orderPlaced(usuario.id,"Se esta procesando un pago por $"+totalAmount.to_s+" de "+thisProduct.nombre)
     rescue Exception => e
       puts "===OPENPAY====ERROR"
       puts e.inspect
       orderPlaced(usuario.id,"No se pudó procesar pago por $"+totalAmount.to_s+" de "+thisProduct.nombre)
       puts "===OPENPAY====ERROR"
     end
    
    respond_to do |format|
      format.json {render json: "OK".to_json}
    end
  end

  # GET /campaigns/1
  # GET /campaigns/1.json
  def show
  end

  # GET /campaigns/new
  def new
     if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @campaign = Campaign.new
  end

  # GET /campaigns/1/edit
  def edit
     if session[:cuser]==nil
      redirect_to "/"
      return
    end
  end



  def getRankedCampaigns
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    # se busca la campaña que tenga mas de 7 dias y este activa y tenga
    # obtener todas las campañas activas   
    campanas=Campaign.where("stoprewardsdate>=?",DateTime.now)
    # ahora seleccionamos los id de las campañas
    idCampanas=campanas.index_by(&:id).keys
    # seleccionar las campañas por orden
    cuentas=Beaconlog.where(:campaing_id=>idCampanas).group(:campaing_id).order('count_all DESC').count
    finalCount=[]
    cuentas.each do |singleAccount|
      hashProp=Hash.new
      puts "my camp:"+singleAccount[0].to_s
      hashProp["nombre"]=Campaign.find(singleAccount[0]).nombre
      hashProp["total"]=singleAccount[1]
      hashProp["id"]=singleAccount[0]
      finalCount.push(hashProp)
    end


    respond_to do |format|
        format.json { render json: finalCount.to_json  }
    end
  end


  # POST /campaigns
  # POST /campaigns.json
  def create
     if session[:cuser]==nil
      redirect_to "/"
      return
    end

    
    if params[:campaign][:imagen]!=nil
      puts params[:campaign][:imagen].original_filename
      path = File.join("public/campaigni", params[:campaign][:imagen].original_filename)
      # write the file
      File.open(path, "wb") { |f| f.write(params[:campaign][:imagen].read) }
      # limpiamos
      params[:campaign][:imagen]=params[:campaign][:imagen].original_filename
    end
    @campaign = Campaign.new(campaign_params)
    @campaign.desactivacion=SecureRandom.hex[0..3]
    @campaign.entrances_id=session[:cuser]
    if @campaign.stoprewardsdate==nil or @campaign.stoprewardsdate==""
      @campaign.stoprewardsdate=DateTime.now+7.days
    end
    if @campaign.stoprewardsat==nil or @campaign.stoprewardsat==""
      @campaign.stoprewardsat=0
    end
    respond_to do |format|
      if @campaign.save
        Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
        format.html { redirect_to edit_campaign_path(@campaign), notice: 'Campaign was successfully created.' }
        format.json { render :show, status: :created, location: @campaign }
      else
        format.html { render :new }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /campaigns/1
  # PATCH/PUT /campaigns/1.json
  def update
     if session[:cuser]==nil
      redirect_to "/"
      return
    end
    if params[:campaign][:imagen]!=nil
      puts params[:campaign][:imagen].original_filename
      path = File.join("public/campaigni", params[:campaign][:imagen].original_filename)
      # write the file
      File.open(path, "wb") { |f| f.write(params[:campaign][:imagen].read) }
      # limpiamos
      params[:campaign][:imagen]=params[:campaign][:imagen].original_filename
    end

    respond_to do |format|
      if @campaign.update(campaign_params)
        if @campaign.stoprewardsdate==nil or @campaign.stoprewardsdate==""
            @campaign.stoprewardsdate=DateTime.now+7.days
            @campaign.save
        end
        if @campaign.stoprewardsat==nil or @campaign.stoprewardsat==""
          @campaign.stoprewardsat=0
          @campaign.save
        end
        if @campaign.desactivacion==nil or @campaign.desactivacion==""
          @campaign.desactivacion=SecureRandom.hex[0..5]

          @campaign.save
        end
        Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
        format.html { redirect_to edit_campaign_path(@campaign), notice: 'Campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @campaign }
      else
        format.html { render :edit }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campaigns/1
  # DELETE /campaigns/1.json
  def destroy
    # verificar que la campaña le pertenezca al usuario
    if session[:cuser]==nil
      redirect_to "/"
      return
    end

    if @campaign.entrances_id!=session[:cuser]
      if Entrance.find_by_id(session[:cuser]).superlevel<1000
        redirect_to "/"
        return  
      end
    end
    @campaign.destroy
    respond_to do |format|
      Pusher.url = "https://aa50b9b2b70a85ae6cb8:5bba1696c5f58caaba98@api.pusherapp.com/apps/172196"
        Pusher.trigger("incuvaSite", "dashboard", {
          message: "New campaign"
        })
      format.html { redirect_to "/campaigns/new", notice: 'Campaign was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_campaign
      @campaign = Campaign.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def campaign_params
      params.require(:campaign).permit(:nombre, :entrances_id, :impresiones_hechas, :impresiones_vistas, :impresiones_meta, :descripcion, :imagen, :justonce, :tiempo_permanencia,:promocode,:cupon,:desactivacion,:sex,:agefrom,:ageto,:stoprewardsat,:stoprewardsdate,:visibleonsite,:bundle,:onexit,:systemmsg,:price,:duration,:parkinglot)
    end
end
