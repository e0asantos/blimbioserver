class AnalyticsController < ApplicationController
  def show
  end

  def logs
  	@analitica=nil
    supuestoUsuario=Entrance.find_by_id(session[:cuser])
    if supuestoUsuario==nil
      redirect_to "/login"
      return
    end
    if supuestoUsuario.superlevel>=1000
      @analitica=Beaconlog.all
    else
      # escogemos solo las campañas del usuario
      miscampanas=Campaign.where(:entrances_id=>supuestoUsuario.id).index_by(&:id).keys
      @analitica=Beaconlog.where(:campaing_id=>miscampanas)
    end
  end

  def chart
  end

  def destroy
  	Beaconlog.find_by_id(params[:bid]).destroy
  	respond_to do |format|
      format.json { head :no_content }
    end
  end
end
