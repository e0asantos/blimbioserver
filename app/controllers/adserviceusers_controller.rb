class AdserviceusersController < ApplicationController
  before_action :set_adserviceuser, only: [:show, :edit, :update, :destroy]

  # GET /adserviceusers
  # GET /adserviceusers.json
  def index
    @adserviceusers = Adserviceuser.all
  end

  # GET /adserviceusers/1
  # GET /adserviceusers/1.json
  def show
  end

  # GET /adserviceusers/new
  def new
    @adserviceuser = Adserviceuser.new
  end

  # GET /adserviceusers/1/edit
  def edit
  end

  # POST /adserviceusers
  # POST /adserviceusers.json
  def create
    @adserviceuser = Adserviceuser.new(adserviceuser_params)
    @adserviceuser.userid=session[:cuser]
    respond_to do |format|
      if @adserviceuser.save
        format.html { redirect_to @adserviceuser, notice: 'Adserviceuser was successfully created.' }
        format.json { render :show, status: :created, location: @adserviceuser }
      else
        format.html { render :new }
        format.json { render json: @adserviceuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adserviceusers/1
  # PATCH/PUT /adserviceusers/1.json
  def update
    respond_to do |format|
      if @adserviceuser.update(adserviceuser_params)
        format.html { redirect_to @adserviceuser, notice: 'Adserviceuser was successfully updated.' }
        format.json { render :show, status: :ok, location: @adserviceuser }
      else
        format.html { render :edit }
        format.json { render json: @adserviceuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adserviceusers/1
  # DELETE /adserviceusers/1.json
  def destroy
    @adserviceuser.destroy
    respond_to do |format|
      format.html { redirect_to adserviceusers_url, notice: 'Adserviceuser was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adserviceuser
      @adserviceuser = Adserviceuser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adserviceuser_params
      params.require(:adserviceuser).permit(:userid,:beacon)
    end
end
