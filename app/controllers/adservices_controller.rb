class AdservicesController < ApplicationController
  before_action :set_adservice, only: [:show, :edit, :update, :destroy]

  # GET /adservices
  # GET /adservices.json
  def index
    @adservices = Adservice.all
  end

  def getAdByPlace
    hashResponse=Hash.new
    tiempo=Time.now - 6
    imagenes=["http://avantumcomunicacion.com/wp-content/uploads/2014/08/Coca-Cola3.jpg","http://paseoaltozano.com/wp-content/uploads/2015/09/telcel.jpg","http://dbakevlar.com/wp-content/uploads/2015/03/Team-Oracle.jpg","http://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/21/2013/08/Oracle_AC45.png","http://beamk-dimole.rhcloud.com/assets/child.mp4"]
    # Movie.where(['release > ?', DateTime.now])
    cuenta=Adserviceuser.where(['created_at > ?', tiempo]).where(:beacon=>params["adNumber"])
    
    if cuenta.length>0
      hashResponse["media_url"]=imagenes[rand(0..imagenes.length-1)]
      if hashResponse["media_url"].index(".jpg")!=nil or hashResponse["media_url"].index(".png")!=nil
        hashResponse["media_type"]="image"  
      else
        hashResponse["media_type"]="video"
      end
    else
      hashResponse["media_url"]="none"
      hashResponse["media_type"]="none"
    end
    # puts params["adNumber"]
    # if params["adNumber"]==1 or params["adNumber"]=="1"
    #   hashResponse["media_url"]="http://beamk-dimole.rhcloud.com/assets/img/img1.jpg"
    #   hashResponse["media_type"]="none1"
    # elsif params["adNumber"]==2 or params["adNumber"]=="2"
    #   hashResponse["media_url"]="http://beamk-dimole.rhcloud.com/assets/testing.mp4"
    #   hashResponse["media_type"]="none2"
    # end
      
    respond_to do |format|        
        format.json { render json: hashResponse }
    end
  end

  # GET /adservices/1
  # GET /adservices/1.json
  def show
  end

  # GET /adservices/new
  def new
    @adservice = Adservice.new
  end

  # GET /adservices/1/edit
  def edit
  end

  # POST /adservices
  # POST /adservices.json
  def create
    @adservice = Adservice.new(adservice_params)

    respond_to do |format|
      if @adservice.save
        format.html { redirect_to @adservice, notice: 'Adservice was successfully created.' }
        format.json { render :show, status: :created, location: @adservice }
      else
        format.html { render :new }
        format.json { render json: @adservice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adservices/1
  # PATCH/PUT /adservices/1.json
  def update
    respond_to do |format|
      if @adservice.update(adservice_params)
        format.html { redirect_to @adservice, notice: 'Adservice was successfully updated.' }
        format.json { render :show, status: :ok, location: @adservice }
      else
        format.html { render :edit }
        format.json { render json: @adservice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adservices/1
  # DELETE /adservices/1.json
  def destroy
    @adservice.destroy
    respond_to do |format|
      format.html { redirect_to adservices_url, notice: 'Adservice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adservice
      @adservice = Adservice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adservice_params
      params.require(:adservice).permit(:name, :age_from, :age_to, :content_type, :url_content, :genre)
    end
end
