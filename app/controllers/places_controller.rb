class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]

  # GET /places
  # GET /places.json
  def index
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    supuestoUsuario=Entrance.where(:id=>session[:cuser]).last
    if supuestoUsuario.superlevel<1000
      redirect_to "/login"
      return
    end
    @places = Place.all
    
  end

  # GET /places/1
  # GET /places/1.json
  def show
    @beacons=nil 
    # hay que verificar si el usuario no es superusuario
    supuestoUsuario=Entrance.where(:id=>session[:cuser]).last
    if Entrance.where(:id=>session[:cuser]).last.superlevel>=1000
      @beacons=Beacon.where(:activo=>1,:ciudad=>@place.id)
    else
      # seleccionamos los del usuario y los que no tienen propietario
      @beacons=Beacon.where(:activo=>1,:ciudad=>@place.id).where('owner is NULL or owner='+session[:cuser].to_s)
    end
  end

  # GET /places/new
  def new
    @place = Place.new
    @beacons=[]
  end

  # GET /places/1/edit
  def edit
    @beacons=[]
  end

  # POST /places
  # POST /places.json
  def create
    @place = Place.new(place_params)

    respond_to do |format|
      if @place.save
        format.html { redirect_to "/places", notice: 'Place was successfully created.' }
        format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to "/places", notice: 'Place was successfully updated.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url, notice: 'Place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
      params.require(:place).permit(:nombre, :pais,:longitude,:latitude,:zoom)
    end
end
