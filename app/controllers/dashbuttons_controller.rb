class DashbuttonsController < ApplicationController
  before_action :set_dashbutton, only: [:show, :edit, :update, :destroy]

  # GET /dashbuttons
  # GET /dashbuttons.json
  def index
    @dashbuttons = Dashbutton.all
  end

  # GET /dashbuttons/1
  # GET /dashbuttons/1.json
  def show
  end

  def attachDash
    # params --- servicio,usuario
    # necesitamos id de servicio y el usuario
    dashb=Dashbutton.where(:id_owner=>session[:cuser]).last
    if dashb!=nil
      dashb.id_servicio=params[:servicio]
      dashb.save
    else 
      dashb=Dashbutton.new
      dashb.id_servicio=params[:servicio]
      dashb.id_owner=session[:cuser]
      dashb.token="2ac0e98f"
      dashb.save
    end

    respond_to do |format|
      
        format.json { render json: dashb.to_json}
      
    end
  end

  def pushDash
    # parametro token
    # en base al token hacemos el llamado
    

    servicioFromDash=Dashbutton.where(:token=>params[:token]).last


    costo=Servicio.where(:id=>servicioFromDash.id_servicio).last
    if costo.description.index("puerta") or costo.description.index("caseta")
      uri = URI('https://lineaccess.mx/anet_devices/sendToDevice')
      https = Net::HTTP.new(uri.host,uri.port)
      https.use_ssl = true
      https.verify_mode = OpenSSL::SSL::VERIFY_NONE
      req = Net::HTTP::Post.new(uri.path,initheader = {'Content-Type' =>'application/x-www-form-urlencoded'})
      req['device'] = 'ce813f6e'
      req['content'] = 'ON'
      req.body="device=ce813f6e&content=ON"

      res = https.request(req)
    end


    recipientCard=Entrance.where(:id=>costo.owner_id).last.confirmation
    currentDude=Entrance.where(:id=>servicioFromDash.id_owner).last
    extractMoneyFrom=currentDude.confirmation
    transaccion=sprintf('%019d', rand(17**17))
    transaccion=transaccion[0..18]
    # transaccion="9144510001324131299"
    puts transaccion

    uri = URI.parse "http://dmartin.org:8001/moneysend/v2/transfer?Format=XML"
    request = Net::HTTP::Post.new uri.path
    t = Time.now                        
    request.body = '<?xml version="1.0" encoding="UTF-8"?>
      <TransferRequest>
         <LocalDate>'+t.strftime("%m%d")+'</LocalDate>
         <LocalTime>'+t.strftime("%H%M%S")+'</LocalTime>
         <TransactionReference>'+transaccion+'</TransactionReference>

         <SenderName>'+currentDude.nombre+'</SenderName>
         <SenderAddress>
            <Line1>Mexico City</Line1>

            <City>Mexico City</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>78437</PostalCode>
            <Country>MEX</Country>
         </SenderAddress>
         <FundingCard>
            <AccountNumber>'+extractMoneyFrom+'</AccountNumber>
            <ExpiryMonth>11</ExpiryMonth>
            <ExpiryYear>2016</ExpiryYear>
         </FundingCard>

         <FundingMasterCardAssignedId>123456</FundingMasterCardAssignedId>
         <FundingAmount>
            <Value>'+costo.price+'</Value>
            <Currency>484</Currency>
         </FundingAmount>
         <ReceiverName>'+Entrance.where(:id=>costo.owner_id).last.nombre+'</ReceiverName>
         <ReceiverAddress>
            <Line1>Coso</Line1>
            <Line2>PO BOX 12</Line2>
            <City>Mexico DF</City>
            <CountrySubdivision>DF</CountrySubdivision>
            <PostalCode>79906</PostalCode>
            <Country>MEX</Country>
         </ReceiverAddress>
         <ReceiverPhone>8180829367</ReceiverPhone>
         <ReceivingCard>
            <AccountNumber>'+recipientCard+'</AccountNumber>
         </ReceivingCard>
         <ReceivingAmount>
            <Value>182206</Value>
            <Currency>484</Currency>
         </ReceivingAmount>
         <Channel>W</Channel>
         <UCAFSupport>true</UCAFSupport>
         <ICA>009674</ICA>
         <ProcessorId>9000000442</ProcessorId>
         <RoutingAndTransitNumber>990442082</RoutingAndTransitNumber>
         <CardAcceptor>
            <Name>My Local Bank</Name>
            <City>Saint Louis</City>
            <State>MO</State>
            <PostalCode>63101</PostalCode>
            <Country>USA</Country>
         </CardAcceptor>
        <TransactionDesc>P2P</TransactionDesc>
        <MerchantId>123456</MerchantId>
      </TransferRequest>'
      request.content_type = 'application/xml'
      response = Net::HTTP.new(uri.host, uri.port).start { |http| http.request request }
      puts response.body.inspect

    respond_to do |format|
      
        format.json { render json: "ok".to_json}
      
    end
  end

  # GET /dashbuttons/new
  def new
    @dashbutton = Dashbutton.new
  end

  # GET /dashbuttons/1/edit
  def edit
  end

  # POST /dashbuttons
  # POST /dashbuttons.json
  def create
    @dashbutton = Dashbutton.new(dashbutton_params)

    respond_to do |format|
      if @dashbutton.save
        format.html { redirect_to @dashbutton, notice: 'Dashbutton was successfully created.' }
        format.json { render :show, status: :created, location: @dashbutton }
      else
        format.html { render :new }
        format.json { render json: @dashbutton.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dashbuttons/1
  # PATCH/PUT /dashbuttons/1.json
  def update
    respond_to do |format|
      if @dashbutton.update(dashbutton_params)
        format.html { redirect_to @dashbutton, notice: 'Dashbutton was successfully updated.' }
        format.json { render :show, status: :ok, location: @dashbutton }
      else
        format.html { render :edit }
        format.json { render json: @dashbutton.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dashbuttons/1
  # DELETE /dashbuttons/1.json
  def destroy
    @dashbutton.destroy
    respond_to do |format|
      format.html { redirect_to dashbuttons_url, notice: 'Dashbutton was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dashbutton
      @dashbutton = Dashbutton.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dashbutton_params
      params.require(:dashbutton).permit(:id_owner, :id_servicio)
    end
end
