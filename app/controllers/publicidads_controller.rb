class PublicidadsController < ApplicationController
  before_action :set_publicidad, only: [:show, :edit, :update, :destroy]

  # GET /publicidads
  # GET /publicidads.json
  def index
    currentUser=Entrance.where(:id=>session[:cuser].to_i).last
    if currentUser.superlevel>=1000
      @publicidads = Publicidad.all
    else
       @publicidads=Publicidad.where(:entrances_id=>session[:cuser].to_i).last
    end
    
  end

  # GET /publicidads/1
  # GET /publicidads/1.json
  def show
  end

  # GET /publicidads/new
  def new
    @publicidad = Publicidad.new
  end

  # GET /publicidads/1/edit
  def edit
  end

  # POST /publicidads
  # POST /publicidads.json
  def create
    if params[:publicidad][:imagen]!=nil
      puts params[:publicidad][:imagen].original_filename
      path = File.join("public/campaigni", params[:publicidad][:imagen].original_filename)
      # write the file
      File.open(path, "wb") { |f| f.write(params[:publicidad][:imagen].read) }
      # limpiamos
      params[:publicidad][:imagen]=params[:publicidad][:imagen].original_filename
    end
    @publicidad = Publicidad.new(publicidad_params)
    @publicidad.entrances_id=session[:cuser].to_i
    respond_to do |format|
      if @publicidad.save
        format.html { redirect_to "/publicidads", notice: 'Publicidad was successfully created.' }
        format.json { render :show, status: :created, location: @publicidad }
      else
        format.html { render :new }
        format.json { render json: @publicidad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /publicidads/1
  # PATCH/PUT /publicidads/1.json
  def update
    if params[:publicidad][:imagen]!=nil
      puts params[:publicidad][:imagen].original_filename
      path = File.join("public/campaigni", params[:publicidad][:imagen].original_filename)
      # write the file
      File.open(path, "wb") { |f| f.write(params[:publicidad][:imagen].read) }
      # limpiamos
      params[:publicidad][:imagen]=params[:publicidad][:imagen].original_filename
    end
    respond_to do |format|
      if @publicidad.update(publicidad_params)
        format.html { redirect_to "/publicidads", notice: 'Publicidad was successfully updated.' }
        format.json { render :show, status: :ok, location: @publicidad }
      else
        format.html { render :edit }
        format.json { render json: @publicidad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publicidads/1
  # DELETE /publicidads/1.json
  def destroy
    @publicidad.destroy
    respond_to do |format|
      format.html { redirect_to publicidads_url, notice: 'Publicidad was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_publicidad
      @publicidad = Publicidad.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def publicidad_params
      params.require(:publicidad).permit(:encabezado, :imagen,:entrances_id)
    end
end
