json.array!(@plastics) do |plastic|
  json.extract! plastic, :id, :card_number, :ccv, :expiration, :user_id, :tokenized
  json.url plastic_url(plastic, format: :json)
end
