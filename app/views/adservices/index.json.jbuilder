json.array!(@adservices) do |adservice|
  json.extract! adservice, :id, :name, :age_from, :age_to, :content_type, :url_content, :genre
  json.url adservice_url(adservice, format: :json)
end
