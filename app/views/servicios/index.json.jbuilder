json.array!(@servicios) do |servicio|
  json.extract! servicio, :id, :uid, :description, :price, :owner_id, :tip, :category
  json.url servicio_url(servicio, format: :json)
end
