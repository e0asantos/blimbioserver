json.array!(@preciohoras) do |preciohora|
  json.extract! preciohora, :id, :hora, :plan, :precio
  json.url preciohora_url(preciohora, format: :json)
end
