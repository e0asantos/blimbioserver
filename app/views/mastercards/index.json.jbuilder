json.array!(@mastercards) do |mastercard|
  json.extract! mastercard, :id
  json.url mastercard_url(mastercard, format: :json)
end
