json.array!(@places) do |place|
  json.extract! place, :id, :nombre, :pais
  json.url place_url(place, format: :json)
end
