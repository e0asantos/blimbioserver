json.array!(@pushes) do |push|
  json.extract! push, :id, :channel, :event, :user_list
  json.url push_url(push, format: :json)
end
