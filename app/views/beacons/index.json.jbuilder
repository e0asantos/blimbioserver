json.array!(@beacons) do |beacon|
  json.extract! beacon, :id, :id, :nombre, :latitud, :longitud, :radio, :uid, :uid_low, :uid_high
  json.url beacon_url(beacon, format: :json)
end
