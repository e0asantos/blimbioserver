json.array!(@dashbuttons) do |dashbutton|
  json.extract! dashbutton, :id, :id_owner, :id_servicio
  json.url dashbutton_url(dashbutton, format: :json)
end
