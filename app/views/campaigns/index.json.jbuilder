json.array!(@campaigns) do |campaign|
  json.extract! campaign, :id, :nombre, :entrances_id, :impresiones_hechas, :impresiones_vistas, :impresiones_meta, :descripcion, :imagen, :justonce, :tiempo_permanencia
  json.url campaign_url(campaign, format: :json)
end
