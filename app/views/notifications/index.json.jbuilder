json.array!(@notifications) do |notification|
  json.extract! notification, :id, :channel, :screen, :dato, :users
  json.url notification_url(notification, format: :json)
end
