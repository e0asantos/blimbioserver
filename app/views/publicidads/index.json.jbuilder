json.array!(@publicidads) do |publicidad|
  json.extract! publicidad, :id, :encabezado, :imagen
  json.url publicidad_url(publicidad, format: :json)
end
