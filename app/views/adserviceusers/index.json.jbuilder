json.array!(@adserviceusers) do |adserviceuser|
  json.extract! adserviceuser, :id, :userid
  json.url adserviceuser_url(adserviceuser, format: :json)
end
