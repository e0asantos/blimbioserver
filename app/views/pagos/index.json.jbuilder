json.array!(@pagos) do |pago|
  json.extract! pago, :id, :fechainicio, :fechafin, :usuarios_id, :estado_payment, :cantidad_original, :cantidad_final, :campaign_id
  json.url pago_url(pago, format: :json)
end
