json.array!(@precios) do |precio|
  json.extract! precio, :id, :nombre, :dias, :precio, :precioperpush
  json.url precio_url(precio, format: :json)
end
