json.array!(@entrances) do |entrance|
  json.extract! entrance, :id, :nombre, :apellido, :email, :empresa, :rfc
  json.url entrance_url(entrance, format: :json)
end
