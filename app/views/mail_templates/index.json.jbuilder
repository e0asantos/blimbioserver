json.array!(@mail_templates) do |mail_template|
  json.extract! mail_template, :id, :template, :mensaje, :titulo
  json.url mail_template_url(mail_template, format: :json)
end
