json.array!(@geofencings) do |geofencing|
  json.extract! geofencing, :id, :nombre, :entrance_id, :area
  json.url geofencing_url(geofencing, format: :json)
end
